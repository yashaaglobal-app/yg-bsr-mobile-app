import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {IonicStorageModule } from '@ionic/storage'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {AuthenticationGuard} from './authentication.guard';
import {DeactivateGuard} from './deactivate.guard';
import { InAppPurchase2 ,IAPProduct} from '@ionic-native/in-app-purchase-2/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule,HttpClientModule, IonicModule.forRoot(),IonicStorageModule.forRoot() ,AppRoutingModule , FormsModule, ReactiveFormsModule ],
  providers: [
    StatusBar,
    SplashScreen,
    AuthenticationGuard,
    DeactivateGuard,
    InAppPurchase2,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
