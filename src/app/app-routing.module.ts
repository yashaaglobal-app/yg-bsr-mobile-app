import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthenticationGuard } from './authentication.guard';
import {DeactivateGuard} from './deactivate.guard';

const routes: Routes = [
  
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    // todo rename as fill daily report
    path: 'fill-report2',
    loadChildren: () => import('./pages/fill-report2/fill-report.module').then( m => m.FillReportPageModule),
    canActivate: [AuthenticationGuard],
    canDeactivate: [DeactivateGuard]
  },

  {
    path: '',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule),
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'registration',
    loadChildren: () => import('./pages/registration/registration.module').then( m => m.RegistrationPageModule),
   
  },
  {
    path: 'add-topic',
    loadChildren: () => import('./pages/add-topic/add-topic.module').then( m => m.AddTopicPageModule),
    canActivate: [AuthenticationGuard],
    
  },
  {
    path: 'add-template',
    loadChildren: () => import('./pages/add-template/add-template.module').then( m => m.AddTemplatePageModule),
    canDeactivate: [DeactivateGuard]
  },
  {
    path: 'edit-template',
    loadChildren: () => import('./pages/edit-template/edit-template.module').then( m => m.EditTemplatePageModule),
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'membership-history',
    loadChildren: () => import('./pages/membership-history/membership-history.module').then( m => m.MembershipHistoryPageModule),
    canActivate: [AuthenticationGuard],
  },
  {
    path: 'select-topic',
    loadChildren: () => import('./pages/select-topic/select-topic.module').then( m => m.SelectTopicPageModule),
    canActivate: [AuthenticationGuard],
     
  },
  {
    path: 'bragshit-report',
    loadChildren: () => import('./pages/bragshit-report/bragshit-report.module').then( m => m.BragshitReportPageModule),
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'bragsheet-report1',
    loadChildren: () => import('./pages/bragsheet-report1/bragsheet-report1.module').then( m => m.BragsheetReport1PageModule),
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'bragsheet-report2',
    loadChildren: () => import('./pages/bragsheet-report2/bragsheet-report2.module').then( m => m.BragsheetReport2PageModule),
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'bragsheet-report-archive',
    loadChildren: () => import('./pages/bragsheet-report-archive/bragsheet-report-archive.module').then( m => m.BragsheetReportArchivePageModule),
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'daily-update',
    loadChildren: () => import('./pages/daily-update/daily-update.module').then( m => m.DailyUpdatePageModule),
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'create-fields',
    loadChildren: () => import('./pages/create-fields/create-fields.module').then( m => m.CreateFieldsPageModule),
    canActivate: [AuthenticationGuard],
    canDeactivate: [DeactivateGuard]
  },
  {
    path: 'create-topic',
    loadChildren: () => import('./pages/create-topic/create-topic.module').then( m => m.CreateTopicPageModule),
    canActivate: [AuthenticationGuard],
    canDeactivate: [DeactivateGuard]
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule),
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'select-template',
    loadChildren: () => import('./pages/select-template/select-template.module').then( m => m.SelectTemplatePageModule),
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'user-profile',
    loadChildren: () => import('./pages/user-profile/user-profile.module').then( m => m.UserProfilePageModule),
    canActivate: [AuthenticationGuard]
  },
  
  {
    path: 'update-basic-info',
    loadChildren: () => import('./pages/update-basic-info/update-basic-info.module').then( m => m.UpdateBasicInfoPageModule),
    canActivate: [AuthenticationGuard]
    //  ,    canDeactivate: [DeactivateGuard]
  },
  {
    path: 'modalpopup',
    loadChildren: () => import('./pages/modalpopup/modalpopup.module').then( m => m.ModalpopupPageModule),
    canActivate: [AuthenticationGuard]
  },
  {
    path: 'bragsheet-report-new',
    loadChildren: () => import('./pages/bragsheet-report-new/bragsheet-report-new.module').then( m => m.BragsheetReportNewPageModule),
    canActivate: [AuthenticationGuard],
    canDeactivate: [DeactivateGuard]
  },
  {
    path: 'view-profile',
    loadChildren: () => import('./pages/view-profile/view-profile.module').then( m => m.ViewProfilePageModule),
    canActivate: [AuthenticationGuard]
  },
// todo 


  {
    path:'',
    
    children: [
      {
        path:'dashboard',
        children:[
          {
            path:'',
               loadChildren: () => import('./pages/dashboard/dashboard.module').then( m => m.DashboardPageModule)
  
          }
        ]
      },
   
  
      {
        path:'user-profile',
        children:[
           {
            path:'',
            loadChildren: () => import('./pages/user-profile/user-profile.module').then( m => m.UserProfilePageModule)
           }
        ]
      },
   
      {
        path:'daily-update',
        children:[
          {
            path:'',
            loadChildren: () => import('./pages/daily-update/daily-update.module').then( m => m.DailyUpdatePageModule)
  
          }
        ]
      }
    
   
  ]
  },
 
  // {
  //   path: 'edit-report-summary',
  //   loadChildren: () => import('./edit-report-summary/edit-report-summary.module').then( m => m.EditReportSummaryPageModule)
  // },
  {
    path: 'forgot-password',
    loadChildren: () => import('./forgot-password/forgot-password.module').then( m => m.ForgotPasswordPageModule)
  },
  // {
  //   path: 'edit-template',
  //   loadChildren: () => import('./edit-template/edit-template.module').then( m => m.EditTemplatePageModule)
  // },
  {
    path: 'confirm-password',
    loadChildren: () => import('./confirm-password/confirm-password.module').then( m => m.ConfirmPasswordPageModule)
  },  {
    path: 'yashaa-membership-plans',
    loadChildren: () => import('./pages/yashaa-membership-plans/yashaa-membership-plans.module').then( m => m.YashaaMembershipPlansPageModule)
  },

 

  // {
  //   path: 'edit-template',
  //   loadChildren: () => import('./pages/edit-template/edit-template.module').then( m => m.EditTemplatePageModule)
  // },



 
  




];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
