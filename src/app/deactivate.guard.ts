import { Injectable } from '@angular/core';
import { CanActivate,CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { from } from 'rxjs';
import {AddTemplatePage} from './pages/add-template/add-template.page';
import {CreateTopicPage} from './pages/create-topic/create-topic.page';
import {CreateFieldsPage} from './pages/create-fields/create-fields.page';
//import {SelectTopicPage} from './pages/select-topic/select-topic.page';
import {FillReportPage} from './pages/fill-report2/fill-report.page';
//import {RegistrationPage} from './pages/registration/registration.page';
import {BragsheetReportNewPage} from './pages/bragsheet-report-new/bragsheet-report-new.page';
import {UpdateBasicInfoPage} from './pages/update-basic-info/update-basic-info.page';

@Injectable({
  providedIn: 'root'
})
export class DeactivateGuard implements CanDeactivate<AddTemplatePage | CreateTopicPage | CreateFieldsPage
| FillReportPage | BragsheetReportNewPage | UpdateBasicInfoPage> {
  canDeactivate( component:AddTemplatePage | CreateTopicPage | CreateFieldsPage |
     FillReportPage | BragsheetReportNewPage | UpdateBasicInfoPage):boolean {
    return window.confirm('Are you sure to exit ?');
  }
  
}
