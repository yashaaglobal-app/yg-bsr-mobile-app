import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';  
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import {ServService} from '../app/services/serv.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  token:any;

  constructor(
    
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private  alertController: AlertController ,
    private router : Router,
    private servService:ServService
  ) {
     this.initializeApp();
  }
 
   initializeApp() {
    this.platform.ready().then(() => {
      // console.log(environment.baseUrl);
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // if page is confirm pass then allow nevigate

      this.token =localStorage.getItem('token');
       if(this.token === null){
        this.router.navigate(['./login']);
       }
       else
       {
        // this.router.navigate(['./dashboard']);
       }
    });
  }

  // setupDeeplinks() {  
  //   this.deeplinks.route({ '/login': 'login' }).subscribe(
  //     match => {
  //       console.log('Successfully matched route', match);
 
  //       // Create our internal Router path by hand
  //       const internalPath = `/${match.$route}/${match.$args['slug']}`;
 
  //       // Run the navigation in the Angular zone
  //       this.zone.run(() => {
  //         this.router.navigateByUrl(internalPath);
  //       });
  //     },
  //     nomatch => {
  //       // nomatch.$link - the full link data
  //       console.error("Got a deeplink that didn't match", nomatch);
  //     }
  //   );
  // }

  async showAlert(title:any,alertMessage:any) {  
    const alert = await this.alertController.create({  
      header: title,   
      message:  alertMessage,  
      buttons: ['OK']  
    });  
    await alert.present();  
    const result = await alert.onDidDismiss();  
    console.log(result);  
  } 
  
  Logout() { // delete token which setted at login page 
     
    localStorage. clear();
    this.initializeApp();
    window.location.assign('/login');
    // this.router.navigate(['./login']);
   // this.router.navigateByUrl('/login');
  }

  Login(){
    window.location.assign('/login');
  }
}
