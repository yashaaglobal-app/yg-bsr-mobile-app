import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import {Router} from "@angular/router";
import {AllServicesService} from '../services/all-services.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.page.html',
  styleUrls: ['./forgot-password.page.scss'],
})
export class ForgotPasswordPage implements OnInit {
  forgotPwd:FormGroup;
  submitted = false;
  constructor(private appComponent: AppComponent,private formBuilder: FormBuilder,private router:Router,private allService:AllServicesService) { }

  ngOnInit() {
    this.forgotPwd= this.formBuilder.group({
      email:new FormControl('',[Validators.required, Validators.email]),
    }
  );
 }
  

 get b6() { return this.forgotPwd.controls; }

 back(){
  this.router.navigate(['/login']);
}


 


 onSubmit() {
     this.submitted = true;
     const formData = new FormData();
     formData.append("email",this.forgotPwd.value.email);
     
     this.allService.forgotPassword(formData).subscribe(
      async (res:any)=>{
        console.log(res);
         if(res.success){
          this.submitted = true;
          this.appComponent.showAlert('success', res.message );
         }
         else{
          this.submitted = false;
          this.appComponent.showAlert('error', res.message );
         }
      }
     )

      // stop here if form is invalid
     if (this.forgotPwd.invalid) {
         return;
     }

   }

 onReset() {
     this.submitted = false;
     this.forgotPwd.reset();
 }



}
