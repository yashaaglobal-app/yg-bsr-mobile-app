import { Component, OnInit } from '@angular/core';
import { AllServicesService } from 'src/app/services/all-services.service';
import { environment } from 'src/environments/environment';
import {Router} from "@angular/router";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-confirm-password',
  templateUrl: './confirm-password.page.html',
  styleUrls: ['./confirm-password.page.scss'],
})
export class ConfirmPasswordPage implements OnInit {
  confirmPwd:FormGroup;
  submitted = false;
  constructor(private appComponent: AppComponent,private formBuilder: FormBuilder,private router:Router,private allService:AllServicesService) { }

  ngOnInit() {
    this.confirmPwd= this.formBuilder.group({
      pass:new FormControl('',[Validators.required]),
      passcnf:new FormControl('',[Validators.required]),
    }
  );
 }
 get b6() { return this.confirmPwd.controls; }

 
 onSubmit() {
  this.submitted = true;

   // stop here if form is invalid
  if (this.confirmPwd.invalid) {
      return;
  }

}

onReset() {
  this.submitted = false;
  this.confirmPwd.reset();
}
}
