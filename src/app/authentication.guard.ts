import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AllServicesService } from 'src/app/services/all-services.service';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {
  
  constructor( private allService : AllServicesService,private router : Router,private alertController : AlertController) { }

  canActivate() : boolean{
    if(this.allService.LoggedIn()){
      return true;
    } else {
      this.showAlert();
      this.router.navigate(['/login']);
      return false
    }
  }
  async showAlert(){
    let alert = await this.alertController.create({
      header :'Unauthorized',
      message :'Enter username and password.',
      buttons :['ok']
    });
    alert.present();
  }
  
}
