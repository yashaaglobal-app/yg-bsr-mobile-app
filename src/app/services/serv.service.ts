import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import {environment} from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ServService {
  bu:any;
  constructor(private posthttp:HttpClient) {

   }

       

  post(serviceName: string, data: any) {
    /* const headers = new HttpHeaders(); */
    const headers = new HttpHeaders();
    headers.append('Access-Control-Allow-Origin', '*');
    headers.append('Access-Control-Allow-Headers', 'Content-Type');
    //headers.append('Access-Control-Allow-Methods', 'GET, POST');
    headers.append('Content-Type', 'multipart/form-data');
    // console.log("headers ", headers);
    const options = { headers: headers, withCredintials: false };
    const url = environment.baseUrl + serviceName;
    return this.posthttp.post(url, data);
    }

    get(serviceName: string,data:any) {
      //const headers = new HttpHeaders();
      //const options = { headers: headers, withCredintials: false };
      const url = environment.baseUrl + serviceName;
      return this.posthttp.get(url,data);
    }
}
