import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Observable  } from 'rxjs';
import {map, take} from 'rxjs/operators'
import { AllServicesService } from './all-services.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {
  constructor( private router : Router , private auth : AllServicesService , private altctrl : AlertController){}
  canActivate( route : ActivatedRouteSnapshot){

    return this.auth.user.pipe(
      take(1),
      map(user=>{
        console.log('log',user)
        if(user){
          return true;
        }
        else{
          this.showAlert();
          return this.router.parseUrl('/sample1')
        }
      })
    )
     
    

  }
  async showAlert(){
    let alert = await this.altctrl.create({
      header :'un-Authorizeed',
      message :'You are not Athourized to visist the page',
      buttons :['ok']
    });
    alert.present();
  }
  
}
