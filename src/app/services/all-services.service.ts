import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import {ServService} from './serv.service';
import { Storage} from '@ionic/storage';
import { HttpClient, HttpHeaders } from '@angular/common/http';


const tokenKey ='user-access'
// const myHeaders = new HttpHeaders();
// myHeaders.append('Access-Control-Allow-Origin', '*');
//myHeaders.append('Access-Control-Allow-Headers', 'Content-Type');
//headers.append('Access-Control-Allow-Methods', 'GET, POST');
//myHeaders.append('Content-Type', 'multipart/form-data');
@Injectable({
  providedIn: 'root'
})
export class AllServicesService {
 
   
  user : Observable<any>;
  private authState = new BehaviorSubject(null)

  constructor(private httpService: ServService,private router: Router , private storage : Storage , private http : HttpClient) {
     this.user = this.authState.asObservable();
    }
 
    login(authCredentials) { //login funtion used at login Page
      return this.httpService.post('login/login_mobile', authCredentials);
    }
     //Helper Methods

  setToken(token: string) { //get token which setted at login page after login
    localStorage.setItem('token', token);
  }
   
  LoggedIn() { 
    return !!localStorage.getItem('token');
  }

deleteToken() { // delete token which setted at login page 
  localStorage.removeItem('token');
}


// getUserPayload() { // not in use
//   var token = this.getToken();
//   if (token) {
//     var userPayload = atob(token.split('.')[1]);
//     return JSON.parse(userPayload);
//   }
//   else
//     return null;
// }

// isLoggedIn() { //not in use
//   var userPayload = this.getUserPayload();
//   if (userPayload)
//     return userPayload.exp > Date.now() / 1000;
//   else
//     return false;
// }

//   logIn(credentials) : Observable<any>{ //not in use 
//     let username = credentials.username;
//     let pass = credentials.pass
//     let user = null;
//     if(username=='admin' &&  pass =='admin'){
      
//       user = { username , role:'Admin'}
    
//     this.authState.next(user);
//     this.storage.set(tokenKey , user);
//     return of(user)
    
//   }
// }

  addTemplate(postData: any): Observable<any> {
    return this.httpService.post('YashaaUserCreateTemplate/userAddTemplate', postData);
  }

  saveTopic(postData:any): Observable<any>{
    return this.httpService.post('YashaaSelectTopic/addTopics',postData)
  }

  reportArchive(postData:any): Observable<any>{
    return this.httpService.post('YashaaReport/archiveReport',postData)
  }

  createTopic(postData: any):Observable<any>{
    return this.httpService.post('YashaaUserAddTopic/userAddTopics',postData);
  }

  getTopics(getData: any):Observable<any>{
    return this.httpService.get('YashaaSelectTopic/?template_id='+localStorage.getItem('templateCreateId')+'&token='+localStorage.getItem('token'),getData);
  }

  getTopicType(getData:any):Observable<any>{
    return this.httpService.get('YashaaUserAddTopic?template_id='+localStorage.getItem('templateCreateId')+'&token='+localStorage.getItem('token'),getData);
  }

  getTemplate(templateType: any):Observable<any>{
    return this.httpService.get('YashaaUserSelectTemplate?type='+templateType+'&token='+localStorage.getItem('token'),templateType);
  }

  getEditableTemplate(template_id: any,template_type:any):Observable<any>{
    return this.httpService.get('YashaaUserSelectTemplate/editTemplate/?template_id='+template_id+'&type='+template_type+'&token='+localStorage.getItem('token'),template_id);
  }

  saveEditedTemplate(postData: any):Observable<any>{
    return this.httpService.post('YashaaUserSelectTemplate/updateTemplate',postData);
  }

  createField(postData: any):Observable<any>{
    return this.httpService.post('YashaaAddFields/addFields',postData);
  }
  getFields(getData:any):Observable<any>{
    return this.httpService.get('YashaaAddFields/?topic_id='+localStorage.getItem('topicId')+'&token='+localStorage.getItem('token'),getData);
    
  }
  userRegistration(postData:any ):Observable<any>{ //user Registration Function
    return this.httpService.post('YashaaUserRegistration/userRegistration',postData);
  }

  getFillTemplate(template_id:any):Observable<any>{ 
     return this.httpService.get('YashaaFileReport?template_id='+template_id+'&token='+localStorage.getItem('token'),template_id);
  }
  getUserProfile( getData : any):Observable<any>{ // Fetch basic user Details on user-profile
    return this.httpService.get('YashaaUpdateProfile/userProfile?token='+localStorage.getItem('token'),getData);
  }
  updateUserProfile( postData : any):Observable<any>{ // Update basic user Details on user-profile
    return this.httpService.post('YashaaUpdateProfile/userUpdateProfile',postData);
  }

  updateUserKeys( postData : any):Observable<any>{ // Update basic user Details on user-profile
    return this.httpService.post('YashaaUserRegistration/store_keys',postData);
  }

  getArchiveBragsheet( getData : any):Observable<any>{  // get Archive Bragsheet Report 
    return this.httpService.get('YashaaReport/archive?token='+localStorage.getItem('token'),getData);
  } 

  getActiveBragsheet( getData : any):Observable<any>{  // get Active Bragsheet Report 
    return this.httpService.get('YashaaReport/active?token='+localStorage.getItem('token'),getData);
  } 

  getPdf( filename : any):Observable<any>{  //download pdf on bragsheet-report2
  console.log(filename);
  
    return this.httpService.get('YashaaReport/downloadPDF/'+filename,null);
  }
  //update professional profile
  postUserProffesionalPofile( postData : any):Observable<any>{  
    return this.httpService.post('YashaaUserProfile/getProfileTemplateId?',postData);
  }
  getBragsheetReport(getData : any):Observable<any>{
    return this.httpService.get('YashaaReport/getReportTemplateId',getData);
  }
  
  getLoginHistory( getData : any):Observable<any>{  //Fetch Login History on 
   
    return this.httpService.get('YashaaUserLoginHistory/getLoginHistory?token='+localStorage.getItem('token'),getData);
  }
 
  postDailyUpdateId( postData : any):Observable<any>{  
   
    return this.httpService.post('YashaaFileReport/insertFileReport',postData);
  }

  getViewProfile( getData : any):Observable<any>{  
   
    return this.httpService.get('YashaaUserProfile/viewProfile?token='+localStorage.getItem('token'),getData);
  }

  getReportSummary(getData : any,topic_id,daily_update_id):Observable<any>{
    return this.httpService.get('YashaaReportSummary?token='+localStorage.getItem('token')+'&topic_id='+topic_id+'&daily_update_id='+daily_update_id,getData);
  }
  forgotPassword( postData : any):Observable<any>{  
   
    return this.httpService.post('YashaaForgottPassword/userForgottPassword',postData);
  }
  getMembershipDetails():Observable<any>{ 
    return this.httpService.get('YashaaMembership?token='+localStorage.getItem('token'),'');
  }

  getMembershipDetailsNew():Observable<any>{ 
    return this.httpService.get('YashaaMembership/CheckPlan/new?token='+localStorage.getItem('token'),'');
  }

  getMembershipHistory():Observable<any>{ 
    return this.httpService.get('YashaaMembership/findAll?token='+localStorage.getItem('token'),'');
  }
  purchaseMemberShip( postData : any):Observable<any>{ // purchase membership
    return this.httpService.post('YashaaMembership/purchaseMembership',postData);
  }

  checkMissingreceipt( postData : any):Observable<any>{ // purchase membership
    return this.httpService.post('YashaaMembership/checkMissingreceipt',postData);
  }

  getPlans():Observable<any>{ 
    return this.httpService.get('YashaaMembership/getPlans','');
  }
}