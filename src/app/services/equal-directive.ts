import { AbstractControl, NG_VALIDATORS, Validators } from '@angular/forms';
import {Directive, Input} from '@angular/core';

@Directive({
    selector : '[confirmValidator]',
    providers : [{
        provide :NG_VALIDATORS,
        useExisting :confirmPass,
        multi : true,

    }]
})

export class confirmPass implements Validators{
    @Input() confirmValidator : string;
    validate(control : AbstractControl) :{[key: string]:any}| any{
        const controlToCompare = control.parent.get(this.confirmValidator);
        if(controlToCompare && controlToCompare.value !== control.value){
            return {'notEquals' : true}
        }
        return  ;

    }
}