import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { YashaaMembershipPlansPage } from './yashaa-membership-plans.page';

describe('YashaaMembershipPlansPage', () => {
  let component: YashaaMembershipPlansPage;
  let fixture: ComponentFixture<YashaaMembershipPlansPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YashaaMembershipPlansPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(YashaaMembershipPlansPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
