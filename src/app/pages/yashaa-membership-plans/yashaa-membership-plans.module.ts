import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { YashaaMembershipPlansPageRoutingModule } from './yashaa-membership-plans-routing.module';

import { YashaaMembershipPlansPage } from './yashaa-membership-plans.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    YashaaMembershipPlansPageRoutingModule
  ],
  declarations: [YashaaMembershipPlansPage]
})
export class YashaaMembershipPlansPageModule {}
