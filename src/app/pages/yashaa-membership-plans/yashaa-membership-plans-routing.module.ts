import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { YashaaMembershipPlansPage } from './yashaa-membership-plans.page';

const routes: Routes = [
  {
    path: '',
    component: YashaaMembershipPlansPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class YashaaMembershipPlansPageRoutingModule {}
