import { Component, ChangeDetectorRef } from '@angular/core';
import { Platform, AlertController } from '@ionic/angular';
import { InAppPurchase2, IAPProduct } from '@ionic-native/in-app-purchase-2/ngx';
import { AllServicesService } from 'src/app/services/all-services.service';
import { Router } from "@angular/router";
@Component({
  selector: 'app-yashaa-membership-plans',
  templateUrl: './yashaa-membership-plans.page.html',
  styleUrls: ['./yashaa-membership-plans.page.scss'],
})
export class YashaaMembershipPlansPage {

  products: IAPProduct[] = [];
  isPurchase: boolean = false;
  constructor(
    public platform: Platform,
    private router: Router,
    private store: InAppPurchase2,
    private ref: ChangeDetectorRef,
    private alertControllers: AlertController,
    private allService: AllServicesService
  ) {
    this.platform.ready().then(() => {
      this.RegisterProducts();
      // Run some code only when the store is ready to be used
      this.store.ready(() => {
        this.products = this.store.products;
        this.ref.detectChanges();
      });
    });
  }

  RegisterProducts() {
    try {
      // this.allService.getPlans().subscribe((res: any) => {
      //   try {
      //     console.log(res);
      //     if (res && res['plan_data'] && Array.isArray(res["plan_data"])) {
      //       res["plan_data"].forEach(plant => {              
      //         this.store.register({
      //           id: plant.productId,
      //           type: this.store.PAID_SUBSCRIPTION
      //         });
      //       });
      //     } else {
      //       console.log(res);
      //     }
      //   } catch (error) {
      //     console.error(error.message);
      //   }
      // }), (err: any) => {
      //   console.error(err);
      // }

      this.store.register({
                id: 'monthly_renew_sub2',
                type: this.store.PAID_SUBSCRIPTION
              });
      
      this.store.register({
                id: 'yearly_renew_sub2',
                type: this.store.PAID_SUBSCRIPTION
              });        
      
      this.SetupListener();         
      this.store.refresh();
      
    } catch (error) {
      console.log(error.message)
    }
  }

  SetupListener() {

    this.store.when('product').initiated((p: IAPProduct) => {
      this.isPurchase = true;
    });

    this.store.when("product").error((error) => {  
      this.isPurchase = false;
    })

    this.store.when("product")
      .approved((p: IAPProduct) => {
        console.log(p);
        p.verify();
      }).verified((p: IAPProduct) => {
        p.finish();
        console.log(p);
        if (this.isPurchase) {
          const formData = new FormData();
          // appending Form Data
          formData.append("token", localStorage.getItem('token'));
          formData.append("receiptData", JSON.stringify(p));

          this.allService.purchaseMemberShip(formData).subscribe(
            (res: any) => {
              // alert(res.msg);
              this.router.navigate(['/daily-update']);
            }), (err: any) => {
              // alert(err.msg);
            }
        }
        this.isPurchase = false;

      }).cancelled((p: IAPProduct) => {
        console.log('cancelled app:->', p.id);
      });

    // User closed the native purchase dialog
    this.store.when("my_product_id").cancelled((product) => {
      this.isPurchase = false;
    });
  }

  async presentAlert(header, message) {
    const alert = await this.alertControllers.create({
      header,
      message,
      buttons: ['OK']
    })
    await alert.present();
  }

  purchase(product: IAPProduct) {
    this.isPurchase = true;
    this.store.order(product).then(p => {
      console.log('Purchase Succesful' + JSON.stringify(p));
    }).catch(e => {
      this.isPurchase = false;
      console.log('purchase error', e);
    });
  }

  back() {
    this.router.navigate(['/daily-update']);
  }

}
