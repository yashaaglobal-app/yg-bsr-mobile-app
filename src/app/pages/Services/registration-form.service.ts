import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RegistrationForm} from './registration-form.model';


@Injectable({
  providedIn: 'root'
})
export class RegistrationFormService {

  registerationForm : RegistrationForm | {}={};

  registerationform : RegistrationForm[];

  readonly baseUrl = 'http://localhost:3000/register';
  constructor( private http : HttpClient) { }

  postUser( urt :RegistrationForm ){
    return this.http.post(this.baseUrl,urt)
  }
}
