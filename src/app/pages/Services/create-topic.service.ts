import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {CreateTopic} from './create-topic.model'
 @Injectable({
  providedIn: 'root'
})
export class CreateTopicService {
  createdTopic : CreateTopic | {}={};

  creattopic : CreateTopic[];
  readonly baseUrl = 'http://localhost:any/tbl_Name';
  constructor(private http : HttpClient) {  }

  postCreateTopic( crt :CreateTopic ){
    return this.http.post(this.baseUrl,crt)
  }
  getTopicList(){
    return this.http.get(this.baseUrl);
  }
}
