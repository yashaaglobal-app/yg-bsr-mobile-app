
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CreateField } from './create-field.model';

@Injectable({
  providedIn: 'root'
})
export class CreateFieldService {

  createdField : CreateField | {}={};

  creatField : CreateField[];
  readonly baseUrl = 'http://localhost:any/tbl_Name';


  constructor( private http : HttpClient) { }

  postCreateField( crf :CreateField ){
    return this.http.post(this.baseUrl,crf)
  }
  getFieldList(){
    return this.http.get(this.baseUrl);
  }
}
