import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';

const TOKEN_Key ='auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  
  authenticationState = new BehaviorSubject(false);
  constructor( private storage : Storage , private plt : Platform) { 
    this.plt.ready().then(()=>{
      this.checkToken();

    })
  }

  login(){
    return this.storage.set(TOKEN_Key , 'Bearer 123456').then(res =>{this.authenticationState.next(true);
    });

  }

  logout(){
    return this.storage.remove(TOKEN_Key).then(() =>{this.authenticationState.next(false);
    });

  }

  isAuthenticated()
  {
    return this.authenticationState.value;
  }
  checkToken(){
    return this.storage.get(TOKEN_Key).then(res=>{
      if(res){
        this.authenticationState.next(true);
      }
    })

  }
}
