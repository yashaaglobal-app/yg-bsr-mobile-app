import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FillReportPage } from './fill-report.page';

describe('FillReportPage', () => {
  let component: FillReportPage;
  let fixture: ComponentFixture<FillReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FillReportPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FillReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
