import { Component, OnInit,ViewChild} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import {AllServicesService} from '../../services/all-services.service';
import {Router} from "@angular/router";
import {AppComponent} from '../../app.component';
import { IonSlides} from '@ionic/angular';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-fill-report',
  templateUrl: './fill-report.page.html',
  styleUrls: ['./fill-report.page.scss'],
})
export class FillReportPage implements OnInit {
  @ViewChild('mySlider')  slides: IonSlides;
  FillTemplate:FormGroup;
  templateData:any;
  topicData:any;
  options:any;

  constructor(private appComponent:AppComponent,private formBuilder: FormBuilder,
    private allService:AllServicesService,private router:Router) {
       this.fillTemplate(environment.template_id);
     }

  ngOnInit() {
    this.FillTemplate=this.formBuilder.group({
      text:new FormControl(),
      datee:new FormControl(),
      rate:new FormControl(),
      range:new FormControl()
    });
    //this.FillTemplate.controls.templatename.setValue(environment.templateName);
  }
  
   
    fillTemplate(template_id:any){
       
       
      const formData = new FormData();
      this.allService.getFillTemplate(template_id).subscribe(
        (res:any)=>{
         // console.log("API Responce"+JSON.stringify(res))
          if(res.status=='true'){
            // alert(res.message);
             this.templateData=res.templateData;
             this.topicData=res.topicData;
             //environment.template_id=res.templateData;
             console.log(res.topicData.fields);
           
          }
          else{
            // alert(res.message);
            this.templateData=res.templateData;
            this.topicData=res.topicData;
            this.options=res.options; 
            console.log(res.topicData.fields);
          }
        }
      )
    
    
  }
 next(){
  this.slides.slideNext();
 }
 prev(){
   this.slides.slidePrev();
 }
  
  
}
