import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { FillReportPageRoutingModule } from './fill-report-routing.module';

import { FillReportPage } from './fill-report.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FillReportPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [FillReportPage]
})
export class FillReportPageModule {}
