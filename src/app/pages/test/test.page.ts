import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,FormControl, FormArray, Validators } from '@angular/forms';

const form_template = [
  {
    "type":"textBox",
    "label":"Name",
  },
  {
    "type":"number",
    "label":"Age"
  },
  {
    "type":"date",
    "label":"date_of_birth"
  },
  {
    "type":"select",
    "label":"favorite book",
    "options":["Jane Eyre","Pride and Prejudice","Wuthering Heights"]
  }
]
@Component({
  selector: 'app-test',
  templateUrl: './test.page.html',
  styleUrls: ['./test.page.scss'],
})


export class TestPage implements OnInit {
  myFormGroup:FormGroup;
  formTemplate:any = form_template; 
  submitted = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {

    let group={}    
    form_template.forEach(input_template=>{
      group[input_template.label]=new FormControl('');  
    })
    this.myFormGroup = new FormGroup(group);

    
     
}

onSubmit(){
  console.log(this.myFormGroup.value);
}

/*get f() { return this.dynamicForm.controls; }
get t() { return this.f.tickets as FormArray; }

onChangeTickets(e) {
  const numberOfTickets:any = e.target.value; 
  alert("no of "+numberOfTickets);
 if (this.t.length < numberOfTickets) {
      for (let i = this.t.length; i < numberOfTickets; i++) {
          this.t.push(this.formBuilder.group({
              name: ['', Validators.required],
              email: ['', [Validators.required, Validators.email]]
          }));
      }
  } else {
      for (let i = this.t.length; i >= numberOfTickets; i--) {
          this.t.removeAt(i);
      }
  }
}

onSubmit() {
  this.submitted = true;

  // stop here if form is invalid
  if (this.dynamicForm.invalid) {
      return;
  }

  // display form values on success
  alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.dynamicForm.value, null, 4));
}


onReset() {
  // reset whole form back to initial state
  this.submitted = false;
  this.dynamicForm.reset();
  this.t.clear();
}

onClear() {
  // clear errors and reset ticket fields
  this.submitted = false;
  this.t.reset();
}*/
}
