import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BragsheetSearchResultPageRoutingModule } from './bragsheet-search-result-routing.module';

import { BragsheetSearchResultPage } from './bragsheet-search-result.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BragsheetSearchResultPageRoutingModule
  ],
  declarations: [BragsheetSearchResultPage]
})
export class BragsheetSearchResultPageModule {}
