import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BragsheetSearchResultPage } from './bragsheet-search-result.page';

describe('BragsheetSearchResultPage', () => {
  let component: BragsheetSearchResultPage;
  let fixture: ComponentFixture<BragsheetSearchResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BragsheetSearchResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BragsheetSearchResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
