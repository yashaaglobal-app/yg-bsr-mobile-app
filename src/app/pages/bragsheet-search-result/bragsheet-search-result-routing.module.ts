import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BragsheetSearchResultPage } from './bragsheet-search-result.page';

const routes: Routes = [
  {
    path: '',
    component: BragsheetSearchResultPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BragsheetSearchResultPageRoutingModule {}
