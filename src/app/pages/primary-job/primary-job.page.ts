import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-primary-job',
  templateUrl: './primary-job.page.html',
  styleUrls: ['./primary-job.page.scss'],
})
export class PrimaryJobPage implements OnInit {
  primaryJob:FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.primaryJob= this.formBuilder.group({
      
      name:new FormControl('',[Validators.required ]),
     
    }

  );
  }


  get b3() { return this.primaryJob.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.primaryJob.invalid) {
        return ;
    }

    // display form values on success
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.primaryJob.value, null, 4));
}

onReset() {
  this.submitted = false;
  this.primaryJob.reset();

}
}