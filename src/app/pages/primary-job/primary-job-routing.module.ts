import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrimaryJobPage } from './primary-job.page';

const routes: Routes = [
  {
    path: '',
    component: PrimaryJobPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrimaryJobPageRoutingModule {}
