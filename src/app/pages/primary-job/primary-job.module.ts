import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { PrimaryJobPageRoutingModule } from './primary-job-routing.module';

import { PrimaryJobPage } from './primary-job.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrimaryJobPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PrimaryJobPage]
})
export class PrimaryJobPageModule {}
