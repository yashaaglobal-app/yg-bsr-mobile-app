import { Component, OnInit } from '@angular/core';
import {ModalController} from '@ionic/angular';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { environment } from 'src/environments/environment';
import {AllServicesService} from '../../services/all-services.service';
import {Router} from "@angular/router";
@Component({
  selector: 'app-modalpopup',
  templateUrl: './modalpopup.page.html',
  styleUrls: ['./modalpopup.page.scss'],
})
export class ModalpopupPage implements OnInit {
  Template1:FormGroup;
  templateData:any;
  template_id:any;
  topicData:any;
  thenBlock : any;
  elseBlock: any;
  selectedArray :any = [];
  isSelected=false;
  len:any;
  isSelectAll=true;
  topicArrayCount:any;
  dailyUpdateId:any;
  constructor(private modalcontroller:ModalController,private formBuilder: FormBuilder,private allService:AllServicesService,
    private router:Router,) {
      //this.fillTemplate1(environment.template_id);
      this.fillTemplate1(localStorage.getItem('template_id'));
     }

  ngOnInit() {
     this.len=1;
    this.Template1=this.formBuilder.group({
      selectAll: new FormControl(false)
    });
    
  }
  //function used to push selected topic into SelectArray.
  selectTopic(event){          
    //debugger;       
    let topic_id = event.value;       
     //if event checked is true then push topic id into selectedArray.
    if(event.checked == true){
      this.selectedArray.push(topic_id);
    } 
    else{
      //if any topic is unchecked then pop that topic id from selectedArray and select all should be unchecked.
        this.isSelectAll = false;
        if(this.selectedArray.includes(topic_id)){
         let index = this.selectedArray.indexOf(topic_id);
          this.selectedArray.splice(index, 1);
          console.log(this.selectedArray);
          console.log("indexId : " +this.selectedArray.indexOf(topic_id)+" topic id : "+topic_id);
        }else{
          console.log('no');
        }
         //this.selectedArray.pop(topic_id);
    }
     //calculated selected array length and assign to len for active button.
    this.len =this.selectedArray.length;  
    
    //select all true false.
    if(this.topicArrayCount == this.len )
    {
      this.isSelectAll = true;
    }
    else{
      this.isSelectAll = false;
    }
  }

  //save topics to envirnment.
  save(){
    
  localStorage.setItem("selectedTopic", this.selectedArray);
  console.log(this.selectedArray);
    //environment.selectedTopic = this.selectedArray;
    //localStorage.setItem('selectedTopic',this.selectedArray);
    // alert("routing");
    //this.router.navigate(['./fill-report2']);
     window.location.assign('/fill-report2');
    this.modalcontroller.dismiss();
    //console.log('hi'+environment.selectedTopic)
  }

  //
  onSelectAll(event): void { 
    this.selectedArray = [];
    let allarray:any=[];
    //event.target.cheked return true or false.
    const checked = event.target.checked;
     //select all check and uncheck checkboxes.
    this.topicData.forEach(item => item.isSelected = checked);
     //when select all check box is checked true then all topic_guid pushed in all array and all array assign to selected array.
    this.topicData.forEach(function (value) { 
      if(checked == true){
      allarray.push(value.topic_guid);  
      }   
      else{
        allarray.pop(value.topic_guid);
       
      }
  });
  this.selectedArray=allarray;
  this.len =this.selectedArray.length;  
  }
  
  //close modal
  CloseModal()
  {
    this.modalcontroller.dismiss();
  }
 
 //get response and push selected topic into array.
  fillTemplate1(template_id: any) 
  {
    let select:any=[];
    const formData = new FormData();
    this.allService.getFillTemplate(template_id).subscribe(
      (res:any)=>{
        //for select all functionality we required 'isSelected':true so we included using foreach loop and push topic guid & name into array.
         if(res.success ){
           res.topicData.forEach(function (value) { 
              select.push({['topic_guid']: value.topic_guid,['topic_name']: value.topic_name,['isSelected']:true});
                
        });
            this.topicData=select;
            //by default i want all checkbox topic_guid on model load to checked all.
            let allarray:any=[];
            const checked =true;
            this.topicData.forEach(function (value) { 
              if(checked == true){ 
                allarray.push(value.topic_guid);  
              }   
              else{
                allarray.pop(value.topic_guid);
              }
          });
          this.selectedArray=allarray;
          this.topicArrayCount =this.selectedArray.length;
        }
        else{
       
        }
      }
    )
  }
}
