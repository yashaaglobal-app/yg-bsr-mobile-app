import { AllServicesService } from 'src/app/services/all-services.service';
import { environment } from 'src/environments/environment';
import {Router} from "@angular/router";
import { Component, OnInit } from '@angular/core';
import { Pipe, PipeTransform } from "@angular/core";

import * as JSEncrypt from '../../../assets/js/jsencrypt/jsencrypt.min.js';

@Component({
  selector: 'app-view-profile',
  templateUrl: './view-profile.page.html',
  styleUrls: ['./view-profile.page.scss'],
})
@Pipe({ name: "texttransform" })
export class ViewProfilePage implements OnInit {
  crypt:any;
  constructor(private allService : AllServicesService,private router:Router) { }
profileData:any;

  ngOnInit() {
    this.crypt = new JSEncrypt();
    this.crypt.setPrivateKey(localStorage.getItem('privKey'));
    this.populateProfileData();
  }
 
  back(){
    this.router.navigate(['/user-profile']);
  }
  populateProfileData(){
    this.allService.getViewProfile(FormData).subscribe(
      (res:any)=>{
        var temp=[];
         if(res.success){
          this.profileData =res.profile_data;
          console.log(this.profileData);
        }
        else{
          
        }
      }
     )
  }
  
  // transform(value: string): string {
  //   const splitBy = "::";
  //   if(value!='' && value!=null) {
  //     const splittedText = value.split(splitBy);
  //     if (splittedText.length > 1) {
  //         return `${"Start Date:" +
  //         splittedText[0] +
  //         " <br/> End Date:" +
  //         splittedText[1]}`;
  //     } else {
  //       return `${splittedText[0]}`;
  //     }
  //   } else {
  //     return value;
  //   }
  // }

  YashaaDecrypt(crypted,field_type_guid) {

    const splitBy = "::";
    if(crypted!='' && crypted!=null) {
      const splittedText = crypted.split(splitBy);
      var crypted1 = `${splittedText[0]}`;

      if(splittedText.length > 1) {
        return `${"Start Date:" +
          splittedText[0] +
          " <br/> End Date:" +
          splittedText[1]}`;
      }else{
        if(field_type_guid != 3 && field_type_guid != 4) {
          return this.crypt.decrypt(crypted);
        }else{
          return crypted;
        } 
      }

    }

  }

}
