import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { CollecteralDutyPageRoutingModule } from './collecteral-duty-routing.module';

import { CollecteralDutyPage } from './collecteral-duty.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CollecteralDutyPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CollecteralDutyPage]
})
export class CollecteralDutyPageModule {}
