import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-collecteral-duty',
  templateUrl: './collecteral-duty.page.html',
  styleUrls: ['./collecteral-duty.page.scss'],
})
export class CollecteralDutyPage implements OnInit {
  Duty:FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.Duty= this.formBuilder.group({
      
      name:new FormControl('',[Validators.required ]),
     
    }

  );
  }


  get b4() { return this.Duty.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.Duty.invalid) {
        return ;
    }

    // display form values on success
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.Duty.value, null, 4));
}

onReset() {
  this.submitted = false;
  this.Duty.reset();

}
}
