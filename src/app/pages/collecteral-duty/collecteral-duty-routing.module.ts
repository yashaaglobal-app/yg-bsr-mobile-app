import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CollecteralDutyPage } from './collecteral-duty.page';

const routes: Routes = [
  {
    path: '',
    component: CollecteralDutyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CollecteralDutyPageRoutingModule {}
