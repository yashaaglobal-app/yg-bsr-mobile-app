import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CollecteralDutyPage } from './collecteral-duty.page';

describe('CollecteralDutyPage', () => {
  let component: CollecteralDutyPage;
  let fixture: ComponentFixture<CollecteralDutyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollecteralDutyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CollecteralDutyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
