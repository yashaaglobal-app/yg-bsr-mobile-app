import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { CreateFieldsPageRoutingModule } from './create-fields-routing.module';

import { CreateFieldsPage } from './create-fields.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateFieldsPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CreateFieldsPage]
})
export class CreateFieldsPageModule {}
