import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateFieldsPage } from './create-fields.page';

describe('CreateFieldsPage', () => {
  let component: CreateFieldsPage;
  let fixture: ComponentFixture<CreateFieldsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateFieldsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateFieldsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
