import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { AllServicesService } from '../../services/all-services.service';
import { Router } from "@angular/router";
import { AppComponent } from '../../app.component';
import { NavController } from '@ionic/angular';
import { ToastController } from '@ionic/angular';
declare var jQuery: any;
@Component({
  selector: 'app-create-fields',
  templateUrl: './create-fields.page.html',
  styleUrls: ['./create-fields.page.scss'],
})

export class CreateFieldsPage implements OnInit {

  CreateFields: FormGroup;
  submitted = false;
  topicname: any;
  fieldname: any;
  fieldtype: any;
  fieldtypes: any;
  fields:any;
  public anArray: any = [];
  addedField:any = [];
  data = false;
  islist = false;
  isText = false;
  fieldTypeIsDate = false;
  
  constructor(public navCtrl: NavController, private appComponent: AppComponent,private formBuilder: FormBuilder,
      private allService: AllServicesService, private router: Router,private toast : ToastController) { 

   this.CreateFields = this.formBuilder.group({
     topicname: new FormControl(),
     listData: new FormArray([
     ]),
     fieldname: new FormControl('', [Validators.required]),
     fieldtype: new FormControl('', [Validators.required]),
  }
  );
    //populate field types
    this.populateFieldType();
    //get topic name
    this.CreateFields.controls.topicname.setValue(localStorage.getItem('topicName'));
   }

  ngOnInit() {
    this.listData.length;
    console.log(this.listData.length);
    localStorage.setItem('isDataAdded','false');
   }
  
  //field type is list then add and remove list element.
  get listData(): FormArray {
    return this.CreateFields.get('listData') as FormArray;
  }
  
  //add list element.
  Add() { 
    this.listData.push(new FormControl());
    console.log(this.listData.length);
  }
 
  //remove list element.
  remove(i){
    this.listData.removeAt(i);
  }
   //check date is added or not.
  onOptionsSelected(val) {
    if (val == 1) {
      this.islist = true;
      this.isText = false;
    }
    else {
      this.islist = false;
      this.isText = true;
    }
    if(val == 3 || val == 4){
      localStorage.setItem('isDataAdded','true');
    }
  }
  
  get b7() { return this.CreateFields.controls; }

  ionViewDidEnter() {
    let elem = <HTMLElement>document.querySelector(".tab_hide");
    if (elem != null) {
      elem.style.display = 'none';
    }
  }
  
 //enterd field name and type save and add more field name and date. 
  saveMore() {   
    console.log("field:"+this.CreateFields.value.fieldtype);
    this.submitted = true;
    this.addedField.push(this.CreateFields.value.fieldtype);
    if (this.CreateFields.invalid) {
      return;
    }
   //check date field is added not not.
    if (this.CreateFields.value.fieldtype == 3 || this.CreateFields.value.fieldtype == 4)
    { 
      //environment.isDataAdded = true;
      localStorage.getItem('isDataAdded') === 'false'
     }
     
     //valid then append data
    const formData = new FormData();
    console.log(this.CreateFields.value.listData);
    formData.append("topic_id", this.CreateFields.value.topicname);
    formData.append("field-type", this.CreateFields.value.fieldtype);
    formData.append("fname", this.CreateFields.value.fieldname);
    formData.append("topic_name_id", localStorage.getItem('topicId'));
    formData.append("token",localStorage.getItem('token'));
    //append list element and push into array.
    this.CreateFields.value.listData.forEach(element => {
      formData.append("listData[]", element);

    });
   
    //service for getting response succ or fail.
    this.allService.createField(formData).subscribe(
      async(res: any) => {
        console.log("API response : " + JSON.stringify(res));
        if (res.success) {
          this.onReset()
          //window.location.assign('/create-fields');
          let toast = await this.toast.create({
            message: res.message,
            color:'success',
            duration: 1500,
            position: 'top',
            buttons:[{
              text:'OK',
              handler:()=>{
                console.log("OK************");
                
              }
              
            }]
           
          });
          toast.present();
          this.populateFieldType();
          window.location.assign('/create-fields');
        }
        else {
          this.appComponent.showAlert('Create Field', res.message + 'error');
          let toast = await this.toast.create({
            message: res.message,
            color:'success',
            duration: 1500,
            position: 'top',
            buttons:[{
              text:'OK',
              handler:()=>{
                console.log("OK");
              }
              
            }]
           
          });
          toast.present();
          return false;
        }
      },
      (err: any) => {
        console.log(err);
        return false;
      }
    );
    
    
  }
  
  //getting field type from backend and iterate.
  populateFieldType() {
    this.allService.getFields(FormData).subscribe(
      (res: any) => {
         if (res.success == 'true') {
          this.fieldtypes = res.fieldType; 
         }
        else {
          this.fieldtypes = res.fieldType;
          this.fields = res.fields;
        }
      }
    )
  }

 //submit added fields
  onSubmit() {
    console.log('isdate: '+localStorage.getItem('isDataAdded'));
    this.submitted = true;
   //check date field is added or not.
    if(localStorage.getItem('isDataAdded') === 'false'){
      //console.log('isdate: ifs '+environment.isDataAdded);
      this.toast.create({
        message: 'Need at least 1 date field type',
        color:'warning',
        duration: 1500,
        position: 'top',
        buttons:[{
          // text:'OK',
          handler:()=>{
          console.log("OK");
          }
        }]
        }).then((toast)=>{
        toast.present();
        })
       return false;
    }
    else{
      //append data
      const formData = new FormData();
      console.log(this.CreateFields.value.listData);
      formData.append("topic_id", this.CreateFields.value.topicname);
      formData.append("field-type", this.CreateFields.value.fieldtype);
      formData.append("fname", this.CreateFields.value.fieldname);
      formData.append("topic_name_id", localStorage.getItem('topicId'));
      formData.append("token", "asd");
  
      this.CreateFields.value.listData.forEach(element => {
        formData.append("listData[]", element);
      });
   
      //getting response from api and display toaster as oer requirnment.
      this.allService.createField(formData).subscribe(  
        async(res: any) => {
           if (res.success == true) {       
            if (localStorage.getItem('isDataAdded') === 'false') {
              let toast = await this.toast.create({
                message: 'Need at least 1 date field type',
                color:'danger',
                duration: 1500,
                position: 'top',
                buttons:[{
                  text:'OK',
                  handler:()=>{
                    console.log("OK");
                  }
                  
                }]
              });
              toast.present();
              return;              
            }
            else {
                let toast = await this.toast.create({
                message: res.message,
                color:'success',
                duration: 1500,
                position: 'top',
                buttons:[{
                  text:'OK',
                  handler:()=>{
                    console.log("OK");
                  }
                  
                }]
               
              });
              toast.present();
              // this.router.navigate(['/select-topic']);
              window.location.assign('/select-topic');
              localStorage.setItem('isDataAdded' ,'false');
            }
          }
          else {
             let toast = await this.toast.create({
              // message: res.message,
              message:'Field name is required',
              color:'danger',
              duration: 2000,
              position: 'top',
              buttons:[{
                // text:'OK',
                handler:()=>{
                  console.log("OK");
                }
                
              }]
             
            });
            toast.present();
            return false;
          }
        },
        (err: any) => {
          console.log(err);
          return false;
        }
      );
  
    }

  }
  //reset function to clear form.
   onReset() {
    this.submitted = false;
    this.CreateFields.reset();
    this.CreateFields.controls.topicname.setValue(localStorage.getItem('topicName'));

  }
  back(){
    this.router.navigate(['/create-topic']);
  }
}
