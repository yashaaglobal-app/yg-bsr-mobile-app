import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateFieldsPage } from './create-fields.page';

const routes: Routes = [
  {
    path: '',
    component: CreateFieldsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateFieldsPageRoutingModule {}
