import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
 
@Component({
  selector: 'app-education',
  templateUrl: './education.page.html',
  styleUrls: ['./education.page.scss'],
})
export class EducationPage implements OnInit {

  Education:FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.Education= this.formBuilder.group({
      
      college:new FormControl('',[Validators.required ]),
      degree:new FormControl('',[Validators.required ]),
      graduation:new FormControl('',[Validators.required ]),
      year:new FormControl('',[Validators.required ]),
    }

  );
  }


  get b5() { return this.Education.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.Education.invalid) {
        return ;
    }

    // display form values on success
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.Education.value, null, 4));
}

onReset() {
  this.submitted = false;
  this.Education.reset();

}

}
