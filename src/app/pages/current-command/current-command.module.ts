import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { CurrentCommandPageRoutingModule } from './current-command-routing.module';

import { CurrentCommandPage } from './current-command.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CurrentCommandPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CurrentCommandPage]
})
export class CurrentCommandPageModule {}
