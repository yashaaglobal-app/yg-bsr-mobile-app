import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-current-command',
  templateUrl: './current-command.page.html',
  styleUrls: ['./current-command.page.scss'],
})
export class CurrentCommandPage implements OnInit {
  Command:FormGroup;
  submitted = false;
  formatted_date:any;
  dt:any;
  current_date=new Date();
  startDate:any;
  
  constructor(private formBuilder: FormBuilder , private router: Router , private toast : ToastController) { }

  ngOnInit() {
    let current_datetime = new Date()
    this.startDate = (current_datetime.getFullYear() ) + "-" + ("0" + (current_datetime.getMonth() + 1)).slice(-2) + "-" + ("0" + (current_datetime.getDate() + 0)).slice(-2)
    this.Command= this.formBuilder.group({
      command:new FormControl('',[Validators.required ]),
      startDate:new FormControl('',[Validators.required])
       
    }

  );
  }
  get f4() { return this.Command.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.Command.invalid) {
        return ;
    }
    this.router.navigate(['/past-command']).then(async () => {
      let toast = await  this.toast.create({
        message: 'Data Saved successfully',
        duration: 3000,
        position: 'top',
        buttons:[{
          text:'OK',
          handler:()=>{
            console.log("OK");
          }
        }]
      })
      toast.present();
    });
    // display form values on success
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.Command.value, null, 4));
}

onReset() {
  this.submitted = false;
  this.Command.reset();
}
}
