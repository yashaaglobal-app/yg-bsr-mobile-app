import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CurrentCommandPage } from './current-command.page';

const routes: Routes = [
  {
    path: '',
    component: CurrentCommandPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrentCommandPageRoutingModule {}
