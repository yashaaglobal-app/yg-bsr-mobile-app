import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CurrentCommandPage } from './current-command.page';

describe('CurrentCommandPage', () => {
  let component: CurrentCommandPage;
  let fixture: ComponentFixture<CurrentCommandPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CurrentCommandPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CurrentCommandPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
