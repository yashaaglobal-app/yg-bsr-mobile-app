import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-leave',
  templateUrl: './leave.page.html',
  styleUrls: ['./leave.page.scss'],
})
export class LeavePage implements OnInit {
  Leave:FormGroup;
  submitted = false;
  formatted_date:any;
  dt:any;
  fromdate:Date;
  current_date=new Date();
  startDate:any;
  inputText = ' ';
  endDate:any;
  
   data: any[] = [];

  
  

  // fieldType_id: 2
  // fieldName: "reason"
 
  // fieldType_id1: 3
  // fieldName1: "start_Date"
 

   multi : any [] []= [[1,2,3] ,["Reason" ,"StartDate","endDate" ]]


   days: Array<Object> = [
    {key: 1, val: 'Reason'},
    {key: 2 ,val: 'StartDate'},
    {key: 3,val: 'endDate'}
    
];
  constructor(private formBuilder: FormBuilder , private toast : ToastController) { }

  ngOnInit() {


    let current_datetime = new Date()
    this.startDate = (current_datetime.getFullYear() ) + "-" + ("0" + (current_datetime.getMonth() + 1)).slice(-2) + "-" + ("0" + (current_datetime.getDate() + 0)).slice(-2)
    this.Leave= this.formBuilder.group({
      reason:new FormControl('',[Validators.required ]),
      startDate:new FormControl('',[Validators.required]),
      endDate:new FormControl('',[Validators.required])
    }

  );
  }

  get f4() { return this.Leave.controls; }

  async onSubmit() {
    this.submitted = true;

     // stop here if form is invalid
     if (this.Leave.invalid) {
        return ;
     }

    // display form values on success
    console.log(this.inputText);
    console.log(this.startDate);
    console.log(this.endDate);
    let laeveData = {
      reason : this.Leave.controls["reason"].value,
      startDate: this.Leave.controls["startDate"].value,
      endDate: this.Leave.controls["endDate"].value
    }
    this.data.push(laeveData);
    
     {
      let toast = await  this.toast.create({
        message: 'Data Saved successfully',
        duration: 3000,
        position: 'top',
        buttons:[{
          text:'OK',
          handler:()=>{
            console.log("OK");
          }
        }]
      })
      toast.present();
    }
}

onReset() {
  this.submitted = false;
  this.Leave.reset();
}
}
