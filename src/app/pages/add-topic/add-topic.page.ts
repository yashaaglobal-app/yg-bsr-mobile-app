import { Component, OnInit } from '@angular/core';
import { AllServicesService } from 'src/app/services/all-services.service';
import {Router} from "@angular/router";
declare var $: any;
declare var jQuery: any;
@Component({
  selector: 'app-add-topic',
  templateUrl: './add-topic.page.html',
  styleUrls: ['./add-topic.page.scss'],
})
export class AddTopicPage implements OnInit {
  loginHistoy : any;
  hideme ={}; //used for the hide and show Login Details
 
  constructor( private allService : AllServicesService,private router:Router) {
    this.hideme ={};
   }

  ngOnInit() {
      this.fetchLoginHistory();
      //  this.abc1();
      
  }


  
  fetchLoginHistory(){
    const formData = new FormData();
    this.allService.getLoginHistory(formData).subscribe(
      (res:any)=>{
         console.log("API Responce"+JSON.stringify(res))
          this.loginHistoy=res.user_history;
          console.log(this.loginHistoy);
       
      }
    )
  }
  
  back(){
    this.router.navigate(['/dashboard']);
  }

 
  
}
