import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import {ReactiveFormsModule} from '@angular/forms';

import { BragsheetReport2PageRoutingModule } from './bragsheet-report2-routing.module';

import { BragsheetReport2Page } from './bragsheet-report2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BragsheetReport2PageRoutingModule,ReactiveFormsModule
  ],
  declarations: [BragsheetReport2Page]
})
export class BragsheetReport2PageModule {}
