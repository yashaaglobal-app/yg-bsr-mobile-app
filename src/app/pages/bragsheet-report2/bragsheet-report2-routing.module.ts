import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BragsheetReport2Page } from './bragsheet-report2.page';

const routes: Routes = [
  {
    path: '',
    component: BragsheetReport2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BragsheetReport2PageRoutingModule {}
