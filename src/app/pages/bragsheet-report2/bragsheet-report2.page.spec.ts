import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BragsheetReport2Page } from './bragsheet-report2.page';

describe('BragsheetReport2Page', () => {
  let component: BragsheetReport2Page;
  let fixture: ComponentFixture<BragsheetReport2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BragsheetReport2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BragsheetReport2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
