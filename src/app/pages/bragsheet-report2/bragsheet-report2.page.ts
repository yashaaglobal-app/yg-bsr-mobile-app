import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AllServicesService } from 'src/app/services/all-services.service';
import { Router } from "@angular/router";
import { environment } from 'src/environments/environment';
import { ToastController } from '@ionic/angular';

declare var jQuery: any;


@Component({
  selector: 'app-bragsheet-report2',
  templateUrl: './bragsheet-report2.page.html',
  styleUrls: ['./bragsheet-report2.page.scss'],
})
export class BragsheetReport2Page implements OnInit {
  select:FormGroup;

activeReport : any;
public hideMe: boolean = false;
public clickedIndex: number
filename : any;
  constructor(private router:Router, private allService : AllServicesService,private formBuilder: FormBuilder, private toast : ToastController) {
    
   }

   
  ngOnInit() {
    this.select= this.formBuilder.group({
      filename:new FormControl(),
     
     });
      (function ($) {
      $(document).ready(function () {
        // console.log("Hello from jQuery!");
      });

    })(jQuery);
  

    this.fetchActiveBrasheet();
  }
  hide() {
    if (this.hideMe == false) {
      this.hideMe = true;

    } else {
      this.hideMe = false;
    }
  }
  back(){
    this.router.navigate(['/bragsheet-report1']);
  }

  fetchActiveBrasheet(){
    const formData = new FormData();
    this.allService.getActiveBragsheet(formData).subscribe(
      (res:any)=>{
        console.log("API Responce"+JSON.stringify(res))
        
        this.activeReport = res.arrReport;
        console.log("act",this.activeReport);
      }
    )
  }
  funReportArchive(id:any)
  {
    const formData = new FormData();
    formData.append("report_id",id);
    formData.append("token",localStorage.getItem('token'));
    this.allService.reportArchive(formData).subscribe(
      async (res:any)=>{
        console.log("API response : "+JSON.stringify(res));
        if(res.success){
           let toast = await this.toast.create({
            message: res.message,
            color:'success',
            duration: 1500,
            position: 'top',
            buttons:[{
              text:'OK',
              handler:()=>{
                console.log("OK");
              }
              
            }]
           
          });
          toast.present();
            //this.router.navigate([ '/bragsheet-report2']);
            window.location.assign('/bragsheet-report2');         
        }
        else{
          let toast = await this.toast.create({
            message: res.message,
            color:'red',
            duration: 1500,
            position: 'top',
            buttons:[{
              text:'OK',
              handler:()=>{
                console.log("OK");
              }
            }]
          });
          toast.present();
        }
      }
    );
    console.log(id);
  }
  download(filename: any){
    // const formData = new FormData();
    // formData.append("",this.select.value.filename);
    
    console.log(filename)
    window.location.assign(environment.baseUrl+'/YashaaReport/downloadPDF/'+filename); 
    
    // this.allService.getPdf(filename).subscribe(
    //   (res:any)=>{
    //     console.log(res);
    //     var downloadURL = window.URL.createObjectURL(res);
    //     var link = document.createElement('a');
    //     link.href = downloadURL;
    //     link.download = downloadURL+".pdf";
    //     link.click();

    
    //   },
    //   (err : any)=>{
    //     console.log(err);
    //   }
    // )
  }

  

  onSubmit(){

  }
  // download() {
  //   const url = 'http://localhost/yg-bsr/apicontroller/YashaaReport/downloadPDF/bragsheet_2020_10_13_10_54_19.pdf';
  //   this.fileTransfer.download(url, this.file.dataDirectory + 'file.pdf').then((entry) => {
  //     console.log('download complete: ' + entry.toURL());
  //   }, (error) => {
  //     // handle error
  //   });
  // }

}
