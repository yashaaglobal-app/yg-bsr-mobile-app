import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
@Component({
  selector: 'app-login-history',
  templateUrl: './login-history.page.html',
  styleUrls: ['./login-history.page.scss'],
})
export class LoginHistoryPage implements OnInit {
  addTopic:FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.addTopic= this.formBuilder.group({
      
      name:new FormControl('',[Validators.required ]),
     
    }

  );
  }

  get f4() { return this.addTopic.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addTopic.invalid) {
        return ;
    }

    // display form values on success
    alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.addTopic.value, null, 4));
}

onReset() {
  this.submitted = false;
  this.addTopic.reset();
}

}
