import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { ToastController } from '@ionic/angular';

import { AllServicesService } from 'src/app/services/all-services.service';
import { Router } from '@angular/router';
import { AppComponent } from '../../app.component';

import * as JSEncrypt from '../../../assets/js/jsencrypt/jsencrypt.min.js';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.page.html',
  styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  registration: FormGroup;
  crypt:any;
  pubKey:any;
  privKey:any;

  constructor(private formBuilder : FormBuilder , private toast:ToastController, private allService : AllServicesService , private router : Router, private appComponent : AppComponent) { }

  ngOnInit() {
    this.crypt = new JSEncrypt();
    this.registerForm= this.formBuilder.group({
      fname:new FormControl('',[Validators.required ]),
      username:new FormControl('',[Validators.required]),
      lname:new FormControl('',[Validators.required]),
      email: ['', [Validators.required, Validators.email]],
      phone:new FormControl('',[Validators.required , Validators.pattern("[0-9]{10}$")]),
      pwd:new FormControl('',[Validators.required ,  Validators.minLength(4)]),
      cpwd:new FormControl('',[Validators.required ])
    });
    this.generateKeys();
  }

  get f() { return this.registerForm.controls };
  //  validation for accept no only in phone no field 
   numberOnly(event): boolean { 
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      console.log("restricted .."+charCode);
      return false;
    }
    return true;
  }

  async onSubmit(){ // Used to register data 
    this.submitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return ;
    }
    //appending form data
    const formData = new FormData();
    formData.append("username",this. registerForm.value.username);
    formData.append("fname",this. registerForm.value.fname);
    formData.append("lname",this. registerForm.value.lname);
    formData.append("phone",this. registerForm.value.phone);
    formData.append("email",this. registerForm.value.email);
    formData.append("pass",this. registerForm.value.pwd);
    formData.append("pubkey",this.pubKey);
    formData.append("privkey",this.privKey);

    this.allService.userRegistration(formData).subscribe(
      (res:any)=>{
        console.log("API response : "+JSON.stringify(res));
        if(res['success']){
          // alert(res);
          this.router.navigate(['/login']).then(async () => {
            let toast = await this.toast.create({
              message:res['message'],
              duration: 1500,
              color :'success',
              position: 'top',
              buttons:[{
                text:'OK',
                handler:()=>{
                  // console.log("OK");  
                }
              }]
            });
    toast.present();
          });

          //environment.templateCreateId = res.inserted_template_id;
        }
        else{
          this.router.navigate(['/registration']).then(async () => {
            let toast = await this.toast.create({
              message:res['message'],
              duration: 1500,
              color :'danger',
              position: 'top',
              buttons:[{
                text:'OK',
                handler:()=>{
                  // console.log("OK");  
                }
              }]
            });
    toast.present();
          });

        }
      });
  }

  generateKeys() {
    var keySize = 512;
    this.crypt = new JSEncrypt({default_key_size: keySize});
    this.pubKey = this.crypt.getPublicKey();
    this.privKey = this.crypt.getPrivateKey();
    return true;
  }
}
 





  // onSubmit() {
  //   this.submitted = true;
    

    // stop here if form is invalid
    //  if (this.registerForm.invalid) {
        //  return ;
    // }

    // display form values on success

  //  alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
  //  console.log(this.registerForm.value);


// onReset() {
//   this.submitted = false;
//   this.registration.reset();
// }
 
