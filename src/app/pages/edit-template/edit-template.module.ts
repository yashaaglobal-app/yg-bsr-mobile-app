import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EditTemplatePageRoutingModule } from './edit-template-routing.module';
import { EditTemplatePage } from './edit-template.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EditTemplatePageRoutingModule
  ],
  declarations: [EditTemplatePage]
})
export class EditTemplatePageModule {}
