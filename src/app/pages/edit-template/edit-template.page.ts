import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { AppComponent } from '../../app.component';
import { environment } from 'src/environments/environment';
import { AllServicesService } from '../../services/all-services.service';
import { Router } from "@angular/router";
import { LoadingController, ModalController, Platform } from '@ionic/angular';
import { TemplateValues } from '../../entities/template_values.entity';
import { ToastController } from '@ionic/angular';
import { count } from 'rxjs/operators';
@Component({
  selector: 'app-edit-template',
  templateUrl: './edit-template.page.html',
  styleUrls: ['./edit-template.page.scss'],
})
export class EditTemplatePage implements OnInit {
  TemplateFG: FormGroup;
  name: any; 
  topictype: any;
  topictypes: any;
  level: any;
  templatename: any;
  TemplatesList: [];
  _rateSpecific: [];
  _militaryGeneral: [];
  _Both: [];
  isIndeterminate: boolean;
  buttonActive: boolean = false;
  allTopicGuids: any;
  selectedArray: any = [];
  topicArrayCount: any;
  len: any;
  _length: number;
  editableTemplateName: string;
  arrPushOnselect: any = [];

  existingTemplateTopic: any = [];
  orgExistingTemplateTopic: any;
  isSelectAllChecked: boolean;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private appComponent: AppComponent,
    private allService: AllServicesService,
    private toast: ToastController,
    public loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.buttonActive = false;
    this.showLoader();
    this.TemplateFG = this.formBuilder.group({
      txtIds: new FormArray([]),
      templateId: new FormControl(localStorage.getItem('edit_template_id')),
      token: new FormControl(localStorage.getItem('token')),
    });
    this.editableTemplateName = localStorage.getItem('edit_template_name');
  }
  showHideAutoLoader() {

    // this.loadingController.create({
    //   message: 'This Loader Will Auto Hide in 2 Seconds',
    //   duration: 2000
    // }).then((res) => {
    //   res.present();

    //   res.onDidDismiss().then((dis) => {
    //     console.log('Loading dismissed! after 2 Seconds', dis);
    //   });
    // });

  }
  updateLoader() {
    // this.loadingController.create({
    //   message:"Updating Please Wait...",
    //   spinner:"bubbles"
    // }).then((res) => {
    //   res.present();
    // });
  }
  showLoader() {
    // this.loadingController.create({
    //   message:"Loading Please Wait...",
    //   spinner:"bubbles"
    // }).then((res) => {
    //   res.present();
    // });
  }
  hideLoader() {

    // this.loadingController.dismiss().then((res) => {
    //   console.log('Loading dismissed!', res);
    //   if(!res){
    //     this.hideLoader();
    //   }
    // }).catch((error) => {
    //   console.log('error', error);
    // });

  }
  chbClickEvent(event): void {
    this.buttonActive = true;
    console.log('Click Event Fired ', event.target.checked);

    const txtIdsArray = <FormArray>this.TemplateFG.controls.txtIds;

    //console.log("Before this.existingTemplateTopic ", this.existingTemplateTopic);
    if (event.target.checked) {
      txtIdsArray.push(new FormControl(event.target.value));
      console.log("checked");
      this.arrPushOnselect.push(event.target.value);
    }
    if (!event.target.checked) {
      console.log("Unchecked ", event.target.value);
      let index = txtIdsArray.controls.findIndex(x => x.value == event.target.value)
      if (index !== -1) {
        txtIdsArray.removeAt(index);
      }

      let indexOfExisting = this.existingTemplateTopic.findIndex(x => x == event.target.value)
      if (indexOfExisting !== -1) {
        this.existingTemplateTopic.splice(indexOfExisting, 1);
      }
      let indexArrPush = this.arrPushOnselect.findIndex(x => x == event.target.value)
      if (indexArrPush !== -1) {
        this.arrPushOnselect.splice(indexArrPush, 1);
      }
      console.log("index ", indexOfExisting);
    }

    this._length = txtIdsArray.length;
    var counter = this.existingTemplateTopic.length + this.arrPushOnselect.length;

    if (counter === this.allTopicGuids.length) {
      console.log("isSelectAllChecked ");
      this.isSelectAllChecked = true;
    } else {
      this.isSelectAllChecked = false
    }

    console.log("After this.counter ", counter);
    console.log("After this.existingTemplateTopic ", this.existingTemplateTopic);
    console.log("After this.existingTemplateTopic Len ", this.existingTemplateTopic.length);
    console.log("After this.allTopicGuids ", this.allTopicGuids.length);
    console.log("After txtIdsArray length ", txtIdsArray.length);
    console.log("After txtIdsArray ", txtIdsArray);
  }

  onSelectAll(event): void {
    this.buttonActive = true;
    console.log("check all event fired", event.target.checked);

    this.selectedArray = [];
    let allarray: any = [];
    this.arrPushOnselect = [];
    if (event.target.checked) {
      this.isSelectAllChecked = true
      this.allTopicGuids.forEach(function (value) {
        allarray.push(value);
      });
      this.existingTemplateTopic = allarray;

      this._rateSpecific.forEach(function (value: any) {
        value.isChecked = true
      });

      this._militaryGeneral.forEach(function (value: any) {
        value.isChecked = true
      });

      this._Both.forEach(function (value: any) {
        value.isChecked = true
      });
      //console.log("this._rateSpecific ", this._rateSpecific);

    }
    else {
      this.isSelectAllChecked = false
      this._rateSpecific.forEach(function (value: any) {
        value.isChecked = false
      });

      this._militaryGeneral.forEach(function (value: any) {
        value.isChecked = false
      });

      this._Both.forEach(function (value: any) {
        value.isChecked = false
      });
      this.existingTemplateTopic = [];


    }

    this.selectedArray = allarray;
    this.len = this.selectedArray.length;
    console.log(allarray);
    console.log("this.existingTemplateTopic ", this.existingTemplateTopic);
  }

  getAllEditableTemplates() {
    console.log('API Service called');
    let select_all: any = [];
    this.allService.getEditableTemplate(localStorage.getItem("edit_template_id"), localStorage.getItem("edit_template_type")).subscribe(
      (res: any) => {
        //for select all functionality we required 'isSelected':true so we included using foreach loop and push topic guid & name into array.
        if (res.success) {
          this.hideLoader();
          this._rateSpecific = res.TemplatesList._rateSpecific;
          this._militaryGeneral = res.TemplatesList._militaryGeneral;
          this._Both = res.TemplatesList._Both;
          console.log("this._rateSpecific ", this._rateSpecific);
          console.log("this._militaryGeneral ", this._militaryGeneral);
          console.log("this._Both ", this._Both);
          //let checkBoxList:any = [];
          res.TemplatesList._rateSpecific.forEach(function (value) {
            select_all.push({ ['topic_guid']: value.topic_guid, ['topic_name']: value.topic_name, ['isChecked']: value.isChecked });
          });

          res.TemplatesList._militaryGeneral.forEach(function (value) {
            select_all.push({ ['topic_guid']: value.topic_guid, ['topic_name']: value.topic_name, ['isChecked']: value.isChecked });
          });

          res.TemplatesList._Both.forEach(function (value) {
            select_all.push({ ['topic_guid']: value.topic_guid, ['topic_name']: value.topic_name, ['isChecked']: value.isChecked });
          });
          //this.checkBoxList = select_all;
          this.allTopicGuids = res.TemplatesList._topicGuids;
          let allarray: any = [];
          const checked = true;
          select_all.forEach(function (value) {
            if (value.isChecked == true) {
              allarray.push(value.topic_guid);
            }
            else {

              allarray.pop(value.topic_guid);
            }
          });
          this.selectedArray = allarray;
          this.existingTemplateTopic = res.existingTemplateTopic;
          this.orgExistingTemplateTopic = res.existingTemplateTopic;
          this.topicArrayCount = this.selectedArray.length;
          if (this.existingTemplateTopic.length === this.allTopicGuids.length) {
            this.isSelectAllChecked = true;
          }
          console.log("this.existingTemplateTopic ", this.existingTemplateTopic.length);
          console.log("this.allTopicGuids ", this.allTopicGuids.length);
        }
        else {
          this.hideLoader();
        }
      }
    )
  }

  get b8() { return this.TemplateFG.controls; }
  ionViewDidEnter() {
    let elem = <HTMLElement>document.querySelector(".tab_hide");
    if (elem != null) {
      elem.style.display = 'none';
    }
    this.getAllEditableTemplates();
  }

  // reloadItems(event): void {
  //   this.getAllEditableTemplates();
  //   setTimeout(() => {
  //     event.target.complete();
  //   });
  // }
  onSubmit1() {
    this.updateLoader();
    let templateValues: TemplateValues = this.TemplateFG.value;
    templateValues.txtIds.push(...this.existingTemplateTopic)
    console.log(this.TemplateFG.value);
    const formData = new FormData();
    formData.append("topic_ids", templateValues.txtIds);
    formData.append("templateId", localStorage.getItem('edit_template_id'));
    formData.append("token", localStorage.getItem('token'));

    this.allService.saveEditedTemplate(formData).subscribe(
      async (res: any) => {
        console.log("API response : " + JSON.stringify(res));
        if (res.success) {
          this.hideLoader();
          let toast = await this.toast.create({
            message: res.message,
            color: 'success',
            duration: 1500,
            position: 'top',
            buttons: [{
              text: 'OK',
              handler: () => {
                console.log("OK");
              }
            }]
          });
          toast.present();
          // this.router.navigate(['/select-template']);
          window.location.assign('/select-template')
        }
        else {
          this.hideLoader();
          templateValues.txtIds = [];
          let toast = await this.toast.create({
            message: res.message,
            color: 'danger',
            duration: 1500,
            position: 'top',
            buttons: [{
              text: 'OK',
              handler: () => {
                console.log("OK");
              }
            }]
          });
          toast.present();
        }
      }
    );
  }

  back() {
    //this.router.navigate(['/select-template']);
    window.location.assign('/select-template')
  }
  createTemplate() {
    this.router.navigate(['./add-template']);
    //window.location.assign('/add-template');
  }

}
