import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditTemplatePage } from './edit-template.page';

describe('EditTemplatePage', () => {
  let component: EditTemplatePage;
  let fixture: ComponentFixture<EditTemplatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTemplatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditTemplatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
