import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BragsheetReportNewPage } from './bragsheet-report-new.page';

const routes: Routes = [
  {
    path: '',
    component: BragsheetReportNewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BragsheetReportNewPageRoutingModule {}
