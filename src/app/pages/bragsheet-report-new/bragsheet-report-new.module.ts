import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { BragsheetReportNewPageRoutingModule } from './bragsheet-report-new-routing.module';

import { BragsheetReportNewPage } from './bragsheet-report-new.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BragsheetReportNewPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [BragsheetReportNewPage]
})
export class BragsheetReportNewPageModule {}
