import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BragsheetReportNewPage } from './bragsheet-report-new.page';

describe('BragsheetReportNewPage', () => {
  let component: BragsheetReportNewPage;
  let fixture: ComponentFixture<BragsheetReportNewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BragsheetReportNewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BragsheetReportNewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
