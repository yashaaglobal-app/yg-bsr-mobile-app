import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { AllServicesService } from '../../services/all-services.service';
import { Router } from "@angular/router";
import { environment } from 'src/environments/environment';
import { exit } from 'process';
import { ElementRef } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
declare var jQuery: any;

@Component({
  selector: 'app-fill-report',
  templateUrl: './bragsheet-report-new.page.html',
  styleUrls: ['./bragsheet-report-new.page.scss'],
})
export class BragsheetReportNewPage implements OnInit {

  FillReport:FormGroup;
  templateData: any;
  topicData: any;
  options: any;
  name = 'Angular';
  len: any;
  template_id: any;
  topic_id:any;
  Topic:any=[];
  tempIndex = 0;
  topicIndex =0;
  inputData:any=[];
   token:any;
  //selectT:any;
   constructor(private router:Router, private formBuilder: FormBuilder,
    private allService: AllServicesService, private routerR: Router,private _host: ElementRef,private toast: ToastController) {
    this.template_id = localStorage.getItem('template_id');
    this.topic_id =localStorage.getItem('topicId');     
    this.fillTemplate(this.template_id);
   }

  ngOnInit() {
    this.FillReport = this.formBuilder.group({
      answer: [''],
    }
    );
    this.token = localStorage.getItem('token'); 
    (function ($) {
      $(document).ready(function () {
        // console.log("Hello from jQuery!");
      });
    })(jQuery);
  }

  //get template data and render form.
  fillTemplate(template_id: any) {
     const formData = new FormData();
    this.allService.getFillTemplate(template_id).subscribe(
      (res: any) => {
        if (res.success) {
          this.templateData = res.templateData;
          this.topicData = res.topicData;
          this.options = res.options;
          this.len = res.topicData.length;
        }
        else {          
         
        }
      }
    )
  }

  back(){
    this.router.navigate(['/bragsheet-report1']);
  }
  //clear form
  onReset() {
    this.FillReport.reset();
  }

  //check valid data and submit.
  onSubmit2(id: any) {
    (function ($) {
      var length = $('#len').val();
      console.log("topic_length : "+length);
      $.fn.validate2(id);
      
      var x, y, i, valid = true;
      if (localStorage.getItem("allowContinue2") != 'no') {
        var formdata = $('#dailyUpdate').serialize();
        var self = this;
        localStorage.setItem("successFillReport", "no");
        $.ajax({
          type: "POST",
          url: environment.baseUrl + "YashaaReport/createReport",
          dataType: 'json',
          data: formdata,
          //valid then create report 
          success: function (resp) {
             if (resp.success) {
               //debugger;
              presentToast(resp.message,'success'); 
              localStorage.setItem("successFillReport", "yes");
              window.location.replace("/bragsheet-report2");
            } else {
              presentToast(resp.message, 'danger');
            }
          },
          error: function (xhr, status, error) {
            presentToast(error, 'danger');
          }
        })
      }
      //else alert
      else {
        presentToast("Please submit all the fields.", 'danger');
         
      }

        // toaster function
        async function presentToast(message,color) {
          console.log("hi present toast call");
          const toast = document.createElement('ion-toast');
          toast.message = message;
          toast.duration = 3000;
          toast.position='top';
          toast.color = color;
          document.body.appendChild(toast);
          return toast.present();
        }
        //value empty then add class
        var slideTab=$("ion-button#"+id).parent();
        var slideTabElements=slideTab.find('.input');
        $( slideTabElements ).each(function( index ) {
          console.log("set error:"+ index + ": " + $( this ).val() );
           if( $( this ).val()=="" ){ 
            $( this).addClass("invalid");
          }
          else
          {
            $( this).removeClass("invalid")
          }
        });
        
    })(jQuery);
  }

  //remove class
  removeClass(){
    (function ($) {
      $( this).removeClass("invalid")
    })(jQuery);
  }
  

}
