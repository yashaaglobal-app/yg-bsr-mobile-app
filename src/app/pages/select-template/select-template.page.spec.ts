import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectTemplatePage } from './select-template.page';

describe('SelectTemplatePage', () => {
  let component: SelectTemplatePage;
  let fixture: ComponentFixture<SelectTemplatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTemplatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectTemplatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
