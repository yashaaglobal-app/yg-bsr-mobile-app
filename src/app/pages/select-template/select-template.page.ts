import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { environment } from 'src/environments/environment';
import { AllServicesService } from '../../services/all-services.service';
import { Router } from "@angular/router";
import { LoadingController, ModalController, Platform } from '@ionic/angular';
import { ModalpopupPage } from '../modalpopup/modalpopup.page';

@Component({
  selector: 'app-select-template',
  templateUrl: './select-template.page.html',
  styleUrls: ['./select-template.page.scss'],
})
export class SelectTemplatePage implements OnInit {
  Template: FormGroup;
  submitted = false;
  Stemplate: any;
  templateData: any = [];
  hideme = {};
  topicNames: any;
  topicData: any;
  buttonActive: boolean = false;
  leavePage: boolean = false;
  showData: boolean = false;
  activeMembership: boolean = false;
  membershipStatus: any;
  noDataFond:boolean = false;

  constructor(private formBuilder: FormBuilder,
    private allService: AllServicesService,
    private router: Router,
    private modalcontroller: ModalController,
    private platform: Platform,
    private loadingController: LoadingController) {
    this.hideme = {};

    //this.showLoader();
    this.Template = this.formBuilder.group({
      Stemplate: new FormControl(),
    });
    if (this.leavePage) {
      this.fetchTemplate(localStorage.getItem("TemplateType"));
    } 
  }

  ngOnInit() {
    this.showData = false;
    console.log("this.showData Init ", this.showData);
    this.getMembershipStatus();
    
  }

  // Get membership status
  getMembershipStatus(){
    this.allService.getMembershipDetails().subscribe(
      (res: any) => {
        console.log("API Responce" + JSON.stringify(res))
        this.membershipStatus = res.membsershipStatus;
        if (this.membershipStatus == 1) {
          this.activeMembership = true;
        }
        console.log("act", this.membershipStatus);
      }
    )
  }

  back() {
    this.router.navigate(['/daily-update']);
  }

  edit(template_id: any) {
    window.location.assign('/edit-template');
  }

  //loader added
  ionViewWillLeave() {
    this.showData = false;
    console.log("this.showData Leave ", this.showData);
    this.leavePage = true;
  }
  ionViewDidEnter() {
    console.log("this.showData Enter ", this.showData);
    this.showData = false;
    let resp: any;
    resp = this.fetchTemplate(localStorage.getItem("TemplateType"));
    localStorage.removeItem('templateName');
    localStorage.removeItem('templateCreateId');
    // this.loadingController.create({
    //   message:"Loading Please Wait...",
    //   spinner:"bubbles"
    //   }).then((resp)=>{
    //   resp.present();
    //   setTimeout(function()
    //   {
    //      resp.dismiss();
    //   })
    // })
    //this.fetchTemplate(localStorage.getItem("TemplateType"));
    console.log('ionViewDidEnter');
    let elem = <HTMLElement>document.querySelector(".tab_hide");
    if (elem != null) {
      elem.style.display = 'none';
    }
  }
  showLoader() {

    // this.loadingController.create({
    //   message:"Loading Please Wait...",
    //   spinner:"bubbles"
    // }).then((res) => {
    //   res.present();
    // });

  }
  hideLoader() {

    // this.loadingController.dismiss().then((res) => {
    //   console.log('Loading dismissed!', res);
    //   if(!res){
    //     this.hideLoader();
    //   }
    // }).catch((error) => {
    //   console.log('error', error);
    // });

  }


  //pull-to-refresh
  reloadItems(event): void {
    this.fetchTemplate(localStorage.getItem("TemplateType"));
    setTimeout(() => {
      event.target.complete();
    });
  }
  //modal popup
  OpenModal(template_id: any) {
    console.log(template_id);
    // return;
    localStorage.setItem('template_id', template_id);
    this.modalcontroller.create({ component: ModalpopupPage }).then((modalElement) => {
      modalElement.present();
    })
  }

  //getting tempalte data,topic data.
  arrayToJSONObject(arr) {
    //header
    var keys = arr;

    //vacate keys from main array
    var newArr = arr.slice(1, arr.length);

    var formatted = [],
      data = newArr,
      cols = keys,
      l = cols.length;
    for (var i = 0; i < data.length; i++) {
      var d = data[i],
        o = {};
      for (var j = 0; j < l; j++)
        o[cols[j]] = d[j];
      formatted.push(o);
    }
    return formatted;
  }

  // Get Template Data
  fetchTemplate(templateType: any) {
    this.showLoader();
    const formData = new FormData();
    let str: any;
    let tempArray: any = [];
    this.allService.getTemplate(templateType).subscribe(
      (res: any) => {
        this.showData = true;
        this.hideLoader();
        // this.buttonActive = true;
        if (res.success) {
          this.templateData = res.TemplateData;
          console.log("Template Data", this.templateData.length);
          if (this.templateData.length > 0) {
            this.buttonActive = true;
          }else{
            this.noDataFond = true;
          }
        }
        else {
          this.hideLoader();
          this.showData = true;
          // this.noDataFond = true;
        }
      }
    )
  }


  get f3() { return this.Template.controls; }

  //submit selected template 
  onSubmit(template_id: any) {
    console.log('in', template_id);
    // return;
    this.submitted = true;

    // stop here if form is invalid
    if (this.Template.invalid) {
      return;
    }
    //append valid data.
    const formData = new FormData();
    // console.log(this.Template.value.Stemplate);
    // return;
    localStorage.setItem('template_id', template_id);
    console.log(template_id);

    localStorage.setItem('toFillTemplateId', template_id);
    this.router.navigate(['./fill-report2']);
    //window.location.assign('/fill-report2');

  }

  //clear form
  onReset() {
    this.submitted = false;
    this.Template.reset();
  }

  editTemplate(id: string, _status: string, _template_name: string) {
    console.log(id, _status);
    localStorage.setItem('edit_template_id', id);
    localStorage.setItem('edit_template_type', _status);
    localStorage.setItem('edit_template_name', _template_name);
    localStorage.setItem('templateName', _template_name);
    localStorage.setItem('templateCreateId', id);
    this.router.navigate(['./edit-template']);
    //window.location.assign('/add-template');
  }

  createTemplate() {
    this.router.navigate(['./add-template']);
    //window.location.assign('/add-template');
  }

}
