import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { environment } from 'src/environments/environment';
import {AllServicesService} from '../../services/all-services.service';
import {Router} from "@angular/router";
import {AppComponent} from '../../app.component';
import { ToastController } from '@ionic/angular';
 
@Component({
  selector: 'app-add-template',
  templateUrl: './add-template.page.html',
  styleUrls: ['./add-template.page.scss'],
})
export class AddTemplatePage implements OnInit {
  addTemplate:FormGroup;
  submitted = false;
  name:any;
  activeMembership:Boolean = false;
  membershipStatus:any;
  level:any;
  templateType:any;
   
  constructor(private appComponent:AppComponent, private formBuilder: FormBuilder,private allService:AllServicesService,
    private router:Router , private toast : ToastController) { }

  ngOnInit() {
      this.addTemplate= this.formBuilder.group({      
        name:new FormControl('',[Validators.required ]),
        level:new FormControl(),
      }
    );
    this.templateType = localStorage.getItem('TemplateType').toLowerCase();
    //console.log(this.templateType);
    this.allService.getMembershipDetails().subscribe(
      (res:any)=>{
        console.log("API Responce"+JSON.stringify(res))
        this.membershipStatus = res.membsershipStatus;
        if(this.membershipStatus == 1)
        {
          this.activeMembership = true;
        }
        console.log("act",this.membershipStatus);
      }
    )
  }

  back(){
    this.router.navigate(['/select-template']);
  }
  
  ionViewDidEnter() {
    let elem = <HTMLElement>document.querySelector(".tab_hide");
    if (elem != null) {
      elem.style.display = 'none';
    }
    console.log('***', this.activeMembership);
    
  }

  get f3() { return this.addTemplate.controls; }
 // Create new template function
  createTemplate() {
    this.submitted = true;
    //if data invalid then show error
    if (this.addTemplate.invalid) {
        return ;
    }
    //valid then append data
    const formData = new FormData();
    formData.append("createTemplate",this.addTemplate.value.name);
    formData.append("select",this.templateType); 
    formData.append("token",localStorage.getItem('token'));
 
    //call post service for add template to backend.
    this.allService.addTemplate(formData).subscribe(
      async (res:any)=>{
        console.log(res);
         if(res.success){
          this.submitted = true;
          let toast = await this.toast.create({
            message: res.message,
            color:'success',
            duration: 1500,
            position: 'top',
            buttons:[{
              text:'OK',
              handler:()=>{
                console.log("OK");
              }
            }]
          });
          toast.present();
          //store template inserted_template_id and tempaltename to envirnment for next functionality.
                // environment.templateCreateId = res.inserted_template_id;
                // environment.templateName = this.addTemplate.value.name;
                localStorage.setItem('templateName',this.addTemplate.value.name);
                localStorage.setItem('templateCreateId',res.inserted_template_id);
                // window.location.assign('/select-topic'); 
                //this.router.navigate([ '/select-topic']); 
                window.location.assign('/select-topic');  
      
        }
        else{
           
          let toast = await this.toast.create({
            message: res.message,
            color:'danger',
            duration: 1500,
            position: 'top',
            buttons:[{
              text:'OK',
              handler:()=>{
                console.log("OK");
              }
              
            }]
           
          });
          toast.present();
                 }
      },
      async (error:any) => {
        console.log(error);
        
      }
    );
   
}

//clear form
onReset() {
  this.submitted = false;
  this.addTemplate.reset();
}
}
