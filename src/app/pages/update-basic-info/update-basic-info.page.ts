import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {  FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { AllServicesService } from 'src/app/services/all-services.service';

import * as JSEncrypt from '../../../assets/js/jsencrypt/jsencrypt.min.js';

@Component({
  selector: 'app-update-basic-info',
  templateUrl: './update-basic-info.page.html',
  styleUrls: ['./update-basic-info.page.scss'],
})
export class UpdateBasicInfoPage implements OnInit {
  updateInfo :FormGroup;
  userData: any;
  submitted=false;
  fname:any;
  lname: any;
  email: any;
  phone: any;
  firstName:any;
  lastName:any;
  Email:any;
  Phone:any;
  crypt:any;
  constructor( private allService : AllServicesService , private formBuilder : FormBuilder , private appComponent : AppComponent , private router : Router) { 
    
  }

  ngOnInit() {

    this.crypt = new JSEncrypt();
    this.crypt.setPrivateKey(localStorage.getItem('privKey'));

    this.updateInfo =this.formBuilder.group({
      fname:new FormControl('',[Validators.required ]),
      lname:new FormControl('',[Validators.required ]),
      email:new FormControl('',[Validators.required ]),
      phone:new FormControl('',[Validators.required ]),
    });
      this.firstName=localStorage.getItem('first_name');
      this.lastName=localStorage.getItem('last_name');
      this.Email=localStorage.getItem('email');
      this.Phone=localStorage.getItem('phone');
      this.fetchUserProfile();
      //this.fname.controls  'Test';
      this.updateInfo.controls.fname.setValue(this.YashaaDecrypt(localStorage.getItem('first_name')));
      this.updateInfo.controls.lname.setValue(this.YashaaDecrypt(localStorage.getItem('last_name')));
      this.updateInfo.controls.email.setValue(localStorage.getItem('email'));
      this.updateInfo.controls.phone.setValue(this.YashaaDecrypt(localStorage.getItem('phone')));
   }
 
  fetchUserProfile(){
    const formData = new FormData();
    this.allService.getUserProfile(formData).subscribe(
      res=>{
          this.userData=res;
      }
    )
  }
 
  get b8() { return this.updateInfo.controls; }

  back(){
    this.router.navigate(['/user-profile']);
  }
  onSubmit() { //used to update Basic Information of userInformation
    this.submitted = true;


    if (this.updateInfo.invalid) {
        return ;}

        const formData = new FormData();

        //appending Form Data
        formData.append("fname",this.YashaaEncrypt(this.updateInfo.value.fname));
        formData.append("lname",this.YashaaEncrypt(this.updateInfo.value.lname))
        formData.append("Phone",this.YashaaEncrypt(this.updateInfo.value.phone));
        formData.append("token",localStorage.getItem('token'));
        formData.append("username",localStorage.getItem('username'));

        this.allService.updateUserProfile(formData).subscribe(
          (res:any)=>{
           this.appComponent.showAlert('Updated',res.message);
          //this.router.navigate(['/user-profile']);
          window.location.assign('/user-profile');
          }),

          (err:any)=>
          {
            this.appComponent.showAlert('Failed',err.message);
          }
  }

  YashaaEncrypt(input) {
    return this.crypt.encrypt(input);
  }

  YashaaDecrypt(crypted) {
    return this.crypt.decrypt(crypted);
  }

}




