import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BragshitReportPage } from './bragshit-report.page';

describe('BragshitReportPage', () => {
  let component: BragshitReportPage;
  let fixture: ComponentFixture<BragshitReportPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BragshitReportPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BragshitReportPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
