import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { BragshitReportPageRoutingModule } from './bragshit-report-routing.module';

import { BragshitReportPage } from './bragshit-report.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BragshitReportPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [BragshitReportPage]
})
export class BragshitReportPageModule {}
