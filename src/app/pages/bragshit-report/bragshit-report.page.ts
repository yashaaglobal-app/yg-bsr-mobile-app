import { Component, OnInit,ViewChild} from '@angular/core';
import { FormGroup,FormControl, Validators,FormArray } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import {AllServicesService} from '../../services/all-services.service';
import {Router} from "@angular/router";
import {AppComponent} from '../../app.component';
import { IonSlides} from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { ToastController } from '@ionic/angular';
declare var jQuery: any;
@Component({
  selector: 'app-bragshit-report',
  templateUrl: './bragshit-report.page.html',
  styleUrls: ['./bragshit-report.page.scss'],
})
export class BragshitReportPage implements OnInit {
 

  @ViewChild('mySlider')  slides: IonSlides;
  productFormGroup:FormArray;
  templateData:any;
  topicData:any;
  topicData1:any;
  options:any;
  name = 'Angular';
  len:any;
  x:any;
  y:any;
  i:any;
  template_id: any;
  topic_id:any;
  Topic:any=[];
  tempIndex = 0;
  topicIndex =0;
  inputData:any=[];
  submitted= false;
  product:FormGroup;
  isValidFormSubmitted = null;
  constructor(private appComponent:AppComponent,private formBuilder: FormBuilder,
    private allService:AllServicesService,private router:Router,private toast : ToastController) { 
      this.fillTemplate(localStorage.getItem('template_id')); 
       //console.log(environment.template_id);
    }

  ngOnInit() {
    this.product = this.formBuilder.group({
      answer:new FormControl('',[Validators.required]),
    }
    );
    this.productFormGroup = this.formBuilder.array([]);  
  }
 
  fillTemplate(template_id:any){
    const formData = new FormData();
    this.allService.getFillTemplate(template_id).subscribe(
      (res:any)=>{
        if(res.success){
           this.templateData=res.templateData; 
           //get each field from topic data anad push into form array.        
          res.topicData.forEach(product =>{ 
            console.log('Topic value: '+product.topic_guid);
            console.log("field count: " + product.fields.length);
            product.fields.forEach(field => {
              this.productFormGroup.push(
                this.formBuilder.group({
                  topic : [product.topic_guid],
                  guid: [product.guid],
                  field_type_guid: [field.field_type_guid],
                  field_name: [field.field_name],
                  options: [field.options],
                  answer: null
                })
              );
             console.log(this.productFormGroup.controls);
            });
           
        });
          this.options=res.options; 
          this.len = res.topicData.length;
            console.log('Length : '+this.len);
         
        }
        else{
           
        }
      }
    ) 
}

  get b2() { return this.product.controls; }

  onSubmit2() {
    let isValidFormSubmitted = true;
    let guid:any = [];
    let date:any = [];
    let topic:any =[];
    let answer:any =[];
    this.submitted = true;
    //check if answer is null then is valid form submitted set false.
    this.productFormGroup.value.forEach(function (item) { 
          if(item.answer == null){
            isValidFormSubmitted = false;
            return;
          }
          else{
            if(item.field_type_guid == 3){
                date = item.answer;
            }
            else{
              answer = item.answer;
            }
            topic = item.topic;
           guid = item.guid;
          }
    });
  
    const formData = new FormData();
    formData.append("answer[][]",answer);
    formData.append("Date[][]",date)
    formData.append("token","asd"); 
    formData.append("Date[][]",date);
    formData.append("topic[][]",topic);
    formData.append("type[][]",'1');
    formData.append("guid[][]",guid);
    // this.allService.postCreateReport(formData).subscribe(
    //   (res:any)=>{
    //   console.log("API response : "+JSON.stringify(res));
    //   this.appComponent.showAlert('Updated',res.message);
    //   }),

    //   (err:any)=>
    //   {
    //     this.appComponent.showAlert('Failed',err.message);
    //   }


    if(isValidFormSubmitted){
       console.log('is valid form');
    }
    else{
      console.log('is invalid form');
    }
 
}

onReset() {
  this.submitted = false;
 }
}
