import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BragshitReportPage } from './bragshit-report.page';

const routes: Routes = [
  {
    path: '',
    component: BragshitReportPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BragshitReportPageRoutingModule {}
