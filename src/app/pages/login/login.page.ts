import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import {Router} from "@angular/router";
// import { LoginService } from "../Services/login.service";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ToastController } from '@ionic/angular';
import { AllServicesService } from 'src/app/services/all-services.service';
import { AppComponent } from 'src/app/app.component';
// import { Token } from '@angular/compiler/src/ml_parser/lexer';




@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  login:FormGroup;
  submitted = false;
  username:any;
  pass:any;
  success: boolean;
  public baseUrl: string;
  
  constructor( private http: HttpClient,private router: Router,private formBuilder: FormBuilder, private toast: ToastController
  , private allservice : AllServicesService , private appComponent : AppComponent
    
    )
   {

      this.baseUrl= environment.baseUrl;
     }
  
  ngOnInit()
   {
      //  this.getPermission();
      //  this.getUniqueDeviceID();
      //  alert('IMEI : '+this.getID_UID('IMEI'));

      this.login= this.formBuilder.group({
      
        
        username:new FormControl('',[Validators.required]),
        pass:new FormControl('',[Validators.required]),
      
      }
  
    );
   }
   
   ionViewDidEnter(){ 
   // this.appComponent.initializeApp();
   }
    get b6() { return this.login.controls; }


 onSubmit(){ // used to call Login Api 
  this.submitted = true;
            if (this.login.invalid) {
              console.log("in")
    return ;
                      }

  const formData = new FormData();
  formData.append("username", this.login.value.username);
  formData.append("pass", this.login.value.pass);
  
  //var data = {"username": this.login.value.username, "pass": this.login.value.pass};
  
  this.allservice.login(formData).subscribe(
      async (res:any)=>{
      if(res.success)
      {
        
      this.allservice.setToken(res['data']['token']); // setted token 

      localStorage.setItem('pubKey',res['data']['pub_key']);
      localStorage.setItem('privKey',res['data']['priv_key']);

      let toast = await this.toast.create({
        message: res.msg,
        color:'success',
        duration: 1500,
        position: 'top',
        buttons:[{
          text:'OK',
          handler:()=>{
            console.log("OK");
          }
        }]
      });
      toast.present();
       localStorage.setItem('username',this.login.value.username);
       window.location.assign('/dashboard');  
      }
      else{
        this.router.navigate(['/login']).then(async () => {
          let toast = await this.toast.create({ //toster for wrong Username or Pass
            message:res['msg'],
            duration: 3000,
            color :'danger',
            position: 'top',
            buttons:[{
              // text:'OK',
              handler:()=>{
                // console.log("OK");  
              }
            }]
          });
          toast.present();
        });
      }
    },
    err => {
      console.log("somthing went wrong",err)
    }
  );
  }
// }
//   async onSubmit() {
//     // this.submitted = true;
//     // alert(this.login.value);
//     // return;
//     // stop here if form is invalid

//     if (this.login.invalid) {
//         // return ; //todo uncomment -temp

//     }
//   console.log(this.login.value);
     

//     var formData: any = new FormData();

//     // formData.append("username", "asd");
//     formData.append("pass", this.login.get('pass').value);
//     formData.append("username", this.login.get('username').value);


//     // message:"log in success",

//     this.router.navigate(['/dashboard']).then(async () => {
//               let toast = await this.toast.create({
//                 message:"log in success",
//                 duration: 1500,
//                 color :'success',
//                 position: 'top',
//                 buttons:[{
//                   text:'OK',
//                   handler:()=>{
//                     // console.log("OK");
//                   }
//                 }]
//               });
//       toast.present();
//             });

// //     this.http.post(`${this.baseUrl}/login/login_mobile`, formData).subscribe(
// //       async res => {
       
// //         // this.msg= (JSON.stringify(res));
// //       // alert(this.msg['msg']);

// //       // alert(res['msg']);

// //       if(res['success'])
// //       {

// //         // TODO color green
// //         this.router.navigate(['/dashboard']).then(async () => {
// //         let toast = await this.toast.create({
// //           message:res['msg'],
// //           duration: 1500,
// //           color :'success',
// //           position: 'top',
// //           buttons:[{
// //             text:'OK',
// //             handler:()=>{
// //               // console.log("OK");
// //             }
// //           }]
// //         });
// // toast.present();
// // });

// //       }
// //       else
// //       {
// //         // TODO show red
// //         let toast = await this.toast.create({
// //           message:res['msg'],
// //           color:'danger',
// //           duration: 1500,
// //           position: 'top',
// //           buttons:[{
// //             text:'OK',
// //             handler:()=>{
// //               // console.log("OK");
// //             }
// //           }]
// //         });
// //         toast.present();
// //       }
// // // todo color red



// //       },
// //       err => {
// //         console.log("error");
// //         // this.serverErrorMessages = err.error.message;
// //       }
// //       );
    
//     // this._login2.login(formData).subscribe(
//     //     res=>{
//     //       console.log("api response for login is ");
//     //     }
//     //   );
    

//  }

 
 
}
