import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DailyUpdatePageRoutingModule } from './daily-update-routing.module';

import { DailyUpdatePage } from './daily-update.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DailyUpdatePageRoutingModule
  ],
  declarations: [DailyUpdatePage]
})
export class DailyUpdatePageModule {}
