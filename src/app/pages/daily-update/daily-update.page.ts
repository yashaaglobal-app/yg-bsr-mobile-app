import { Component, ChangeDetectorRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Platform, AlertController } from '@ionic/angular';
import { InAppPurchase2, IAPProduct } from '@ionic-native/in-app-purchase-2/ngx';
import { AllServicesService } from 'src/app/services/all-services.service';
declare var jQuery: any;

@Component({
  selector: 'app-daily-update',
  templateUrl: './daily-update.page.html',
  styleUrls: ['./daily-update.page.scss'],
})
export class DailyUpdatePage {
  products: IAPProduct[] = [];
  membershipStatus: any;
  allowFreeStatus:any;
  activeMembership: Boolean = false;
  showData: boolean = false;
  prodItr:any;
  activePlanOrgTransId:any;
  activePlanPlatform:any;
  scrrenRefreshEvnt:any;
  currPlatform: string = '';

  constructor(
              public platform: Platform,
              private router:Router,
              private allService:AllServicesService,
              private store: InAppPurchase2,
              private ref: ChangeDetectorRef,
             ) { }

  ionViewWillEnter() {

    if (this.platform.is('android')) {
      this.currPlatform = 'ANDROID';
    } else if (this.platform.is('ios')) {
      this.currPlatform = 'IOS';
    }

    this.prodItr=0;
    this.showData = false;

    var this1 = this;

    this1.hide_daily_update_options();

    this.subs_status_check();
  }

  subs_status_check() {

    console.log("Subscription Status Check");

    this.getMembershipStatus().then(
      (res: any) => {
        if(res) {
          this.show_daily_update_options();
          if(this.currPlatform == 'ANDROID') {
            console.log("Refresh Signal for Android Only");
            this.refreshSignal();
          }
        }else{
          this.verify_org_trans_id_and_platform();
        }
      }
    );
  }

  verify_org_trans_id_and_platform() {
    console.log("Verify Platform and Org Transaction ID ===> ", this.activePlanPlatform, this.activePlanOrgTransId);
    if( this.activePlanPlatform != 'ios' && this.activePlanOrgTransId == '' ) {
      this.refreshSignal();
    }else{
      this.check_free();
    }
  }

  check_free() {
    console.log("Check Allow Free ===> ", this.allowFreeStatus);
    if( this.allowFreeStatus == 1 ) {
      this.show_daily_update_options();
    }
  }

  show_daily_update_options() {
    console.log("Show Daily Update Options");
    // this.activeMembership = true;

    (function ($) {
      $("#dailyUpdateOptions").show();
      $("#lockBoxOptions").hide();
    })(jQuery);
  }

  hide_daily_update_options() {
    console.log("Hide Daily Update Options");
    (function ($) {
      $("#dailyUpdateOptions").hide();
      $("#lockBoxOptions").show();
    })(jQuery);
  }

  refreshSignal() {
    console.log("Refresh Signal");
    this.platform.ready().then(() => {
      this.store.register({
        id: 'monthly_renew_sub2',
        type: this.store.PAID_SUBSCRIPTION
      });

      this.store.register({
        id: 'yearly_renew_sub2',
        type: this.store.PAID_SUBSCRIPTION
      });        
      try {
        this.SetupListener();
        this.store.refresh();
      } catch (error) {
        console.log(error.message);
      }
    });
  }

  SetupListener() {

    console.log("Setup Listener");

    this.store.when('product').initiated((p: IAPProduct) => {
      console.log("In Product Initiated", p);
    });

    this.store.when("product").error((error) => {
    });

    this.store.when("product")
      .approved((p: IAPProduct) => {
        console.log('approved() Called');
        p.verify();
      }).verified((p: IAPProduct) => {
        console.log('verified() Called');
        p.finish();
      }).cancelled((p: IAPProduct) => {
        console.log('cancelled app:->',p.id);
      }).owned((p: IAPProduct) => {

        console.log("Iteration ===> "+this.prodItr);

        console.log(JSON.stringify(p));

        if(this.prodItr == 0) {

          // console.log(p);

          const formData = new FormData();

          formData.append("token",localStorage.getItem('token'));
          formData.append("receiptData", JSON.stringify(p));

          this.allService.checkMissingreceipt(formData).subscribe((res: any) => {
            if(res.success) {
              this.subs_status_check();
            }else{
              this.check_free();
            }
          }), (err: any) => {}
        }
        this.prodItr = this.prodItr+1;
      });
  }

  // Get membership status
  getMembershipStatus(){
    return new Promise(resolve=>{
      this.allService.getMembershipDetailsNew().subscribe(
        (res: any) => 
        {
          this.membershipStatus = res.membsershipStatus;
          this.allowFreeStatus  = res.allowFreeStatus;
          this.activePlanOrgTransId = res.original_trans_id;
          this.activePlanPlatform = res.platform;
          console.log("Membership Status : ===> "+this.membershipStatus);
          if (this.membershipStatus == 1) {
            resolve(true);
          }
          else {
            resolve(false);
          }
        }
      )
    })
  }

  ionViewDidEnter() {
    this.showData = true; 
    let elem = <HTMLElement>document.querySelector(".tab_hide");
    if (elem != null) {
      elem.style.display = 'flex';
    }
  } 

  Active() {
    localStorage.setItem('TemplateType','Active');
    this.router.navigate(['/select-template']);
  }

  Reservist() {
    localStorage.setItem('TemplateType','Reservist');
    this.router.navigate(['/select-template']);
  }

  back() {
    this.router.navigate(['/dashboard']);
  }

}
