import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BragsheetReportArchivePageRoutingModule } from './bragsheet-report-archive-routing.module';

import { BragsheetReportArchivePage } from './bragsheet-report-archive.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BragsheetReportArchivePageRoutingModule
  ],
  declarations: [BragsheetReportArchivePage]
})
export class BragsheetReportArchivePageModule {}
