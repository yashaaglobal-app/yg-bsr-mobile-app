import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BragsheetReportArchivePage } from './bragsheet-report-archive.page';

const routes: Routes = [
  {
    path: '',
    component: BragsheetReportArchivePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BragsheetReportArchivePageRoutingModule {}
