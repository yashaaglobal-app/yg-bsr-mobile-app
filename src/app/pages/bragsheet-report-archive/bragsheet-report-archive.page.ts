import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AllServicesService } from 'src/app/services/all-services.service';
import { Router } from "@angular/router";
import { environment } from 'src/environments/environment';
declare var jQuery: any;
@Component({
  selector: 'app-bragsheet-report-archive',
  templateUrl: './bragsheet-report-archive.page.html',
  styleUrls: ['./bragsheet-report-archive.page.scss'],
})
export class BragsheetReportArchivePage implements OnInit {
  public hideMe: boolean = false;
  select:FormGroup;
  archiveReport : any;
  filename : any;
  constructor(private router:Router, private allService : AllServicesService,private formBuilder: FormBuilder) { }
  
  ngOnInit() {
    this.select= this.formBuilder.group({
      filename:new FormControl(),
     });
      
    this.fetchArchiveBrasheet();
  }
  hide() {
    if (this.hideMe == false) {
      this.hideMe = true;

    } else {
      this.hideMe = false;
    }
  }
  back(){
    this.router.navigate(['/bragsheet-report1']);
  }

  fetchArchiveBrasheet(){
    const formData = new FormData();
    this.allService.getArchiveBragsheet(formData).subscribe(
      (res:any)=>{
        console.log("API Responce"+JSON.stringify(res))
        
        this.archiveReport = res.arrReport;
        console.log("act",this.archiveReport);
      }
    )
  }
  download(filename: any){
    console.log(filename)
    window.location.assign(environment.baseUrl+'/YashaaReport/downloadPDF/'+filename); 
  }

  onSubmit(){

  }

}
