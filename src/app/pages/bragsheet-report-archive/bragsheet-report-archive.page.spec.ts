import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BragsheetReportArchivePage } from './bragsheet-report-archive.page';

describe('BragsheetReportArchivePage', () => {
  let component: BragsheetReportArchivePage;
  let fixture: ComponentFixture<BragsheetReportArchivePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BragsheetReportArchivePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BragsheetReportArchivePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
