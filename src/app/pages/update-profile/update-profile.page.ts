import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
@Component({
  selector: 'app-update-profile',
  templateUrl: './update-profile.page.html',
  styleUrls: ['./update-profile.page.scss'],
})
export class UpdateProfilePage implements OnInit {
  updateProfile:FormGroup;
  submitted = false;
  formatted_date:any;
  dt:any;
  fromdate:Date;
  current_date=new Date();
  avlDate:any;
  constructor(private formBuilder: FormBuilder , private router : Router , private toast : ToastController) { }

  ngOnInit() {
    let current_datetime = new Date()
    this.avlDate = (current_datetime.getFullYear() ) + "-" + ("0" + (current_datetime.getMonth() + 1)).slice(-2) + "-" + ("0" + (current_datetime.getDate() + 0)).slice(-2)
    this.updateProfile= this.formBuilder.group({
      
       fname:new FormControl('',[Validators.required ]),
       mname:new FormControl('',[Validators.required]),
       lname:new FormControl('',[Validators.required]),
      //  rate:new FormControl('',[Validators.required]),
      // rank:new FormControl('',[Validators.required]),
      // designator:new FormControl('',[Validators.required]),
      // avlDate:new FormControl('',[Validators.required]),
      //  eaos:new FormControl('',[Validators.required]),

      // fname:new FormControl(),
      // mname:new FormControl(),
      // lname:new FormControl(),
       rate:new FormControl(),
       rank:new FormControl(),
      designator:new FormControl(),
       avlDate:new FormControl(),
       eaos:new FormControl()
    }

  );
  }

  get f1() { return this.updateProfile.controls; }

  async  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.updateProfile.invalid) {
        return ;
    }

    // display form values on success
    this.router.navigate(['/current-command']).then(async () => {
      let toast = await  this.toast.create({
        message: 'Data Saved successfully',
        duration: 3000,
        position: 'top',
        buttons:[{
          text:'OK',
          handler:()=>{
            console.log("OK");
          }
        }]
      })
      toast.present();
    });
    
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.updateProfile.value, null, 4));
}

onReset() {
  this.submitted = false;
  this.updateProfile.reset();
}
}
