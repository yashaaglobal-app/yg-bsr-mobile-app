import { Component, OnInit } from '@angular/core';
import { AllServicesService } from 'src/app/services/all-services.service';
import { environment } from 'src/environments/environment';
import {ModalpopupPage} from '../modalpopup/modalpopup.page';
import {ModalController} from '@ionic/angular';
import {Router} from "@angular/router";
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Storage } from '@ionic/storage';

import * as JSEncrypt from '../../../assets/js/jsencrypt/jsencrypt.min.js';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.page.html',
  styleUrls: ['./user-profile.page.scss'],
})
export class UserProfilePage implements OnInit {
  fname:any;
  userData: any;
  updateData:any;
  profileData:any;
  profile:FormGroup;
  Username:any;
  crypt:any;
  constructor( private formBuilder: FormBuilder,private allService : AllServicesService,private modalcontroller:ModalController,private router:Router, public storage: Storage) { }

  ngOnInit() {
    this.crypt = new JSEncrypt();
    this.crypt.setPrivateKey(localStorage.getItem('privKey'));

    this.Username=localStorage.getItem('username');
    this.fetchUserProfile();
  }

  //append data into input fields.
  fetchUserProfile(){
    const formData = new FormData();
    this.allService.getUserProfile(formData).subscribe(
      (res:any)=>{
        this.userData=res;
        localStorage.setItem("first_name",res[0].user_first_name);
        localStorage.setItem("last_name",res[0].user_last_name);
        localStorage.setItem("email",res[0].email);
        localStorage.setItem("phone",res[0].phone);
      }
    )
  }

  viewProfile(){
      this.router.navigate([ '/view-profile']); 
  }

  back(){
    this.router.navigate(['/dashboard']);
  }

  //on button click modal popup,get response.
  updateProfile(){

   this.allService.postUserProffesionalPofile(FormData).subscribe(
    (res:any)=>{
       if(res.success){
        this.updateData=res;
         localStorage.setItem('template_id',res.profile_template_id);
        this.modalcontroller.create({component:ModalpopupPage}).then((modalElement)=>{
          modalElement.present();
        })
      }
      else{
     
      }
    }
   )
  }

  generateKeys() {
      var keySize = 512;
      var crypt = new JSEncrypt({default_key_size: keySize});
      crypt.getKey(function () {
          // this.storage.set('pubKey', crypt.getPublicKey());
          // this.storage.set('privKey', crypt.getPrivateKey());
      });
      crypt.getKey();
      return true;
  }

  YashaaDecrypt(crypted) {
    return this.crypt.decrypt(crypted);
  }
}
