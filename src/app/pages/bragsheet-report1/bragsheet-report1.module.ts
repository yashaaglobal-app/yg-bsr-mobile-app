import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BragsheetReport1PageRoutingModule } from './bragsheet-report1-routing.module';

import { BragsheetReport1Page } from './bragsheet-report1.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BragsheetReport1PageRoutingModule
  ],
  declarations: [BragsheetReport1Page]
})
export class BragsheetReport1PageModule {}
