import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BragsheetReport1Page } from './bragsheet-report1.page';

describe('BragsheetReport1Page', () => {
  let component: BragsheetReport1Page;
  let fixture: ComponentFixture<BragsheetReport1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BragsheetReport1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BragsheetReport1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
