import { Component, OnInit } from '@angular/core';
import {AllServicesService} from '../../services/all-services.service';
import { environment } from 'src/environments/environment';
import {Router} from "@angular/router";

@Component({
  selector: 'app-bragsheet-report1',
  templateUrl: './bragsheet-report1.page.html',
  styleUrls: ['./bragsheet-report1.page.scss'],
})
export class BragsheetReport1Page implements OnInit {
reportTemplateId:any;
  constructor(private allService:AllServicesService,private router:Router) { }

  ngOnInit() {
  }
   
  //on button click get responce,pass template_id and navigate.
  generateReport(){
      this.allService.getBragsheetReport(FormData).subscribe(
       (res:any)=>{
          if(res.success){
           this.reportTemplateId=res;
          localStorage.setItem('template_id',res.report_template_id) ; 
           this.router.navigate(['./bragsheet-report-new']);
         }
         else{
        
         }
       }
      )
     
  }

  back(){
    this.router.navigate(['/dashboard']);
  }

}


