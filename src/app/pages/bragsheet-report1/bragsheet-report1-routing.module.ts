import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BragsheetReport1Page } from './bragsheet-report1.page';

const routes: Routes = [
  {
    path: '',
    component: BragsheetReport1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BragsheetReport1PageRoutingModule {}
