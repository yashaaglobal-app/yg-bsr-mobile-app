import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateTopicPage } from './create-topic.page';

describe('CreateTopicPage', () => {
  let component: CreateTopicPage;
  let fixture: ComponentFixture<CreateTopicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTopicPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateTopicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
