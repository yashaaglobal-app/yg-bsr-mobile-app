import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { environment } from 'src/environments/environment';
import {AllServicesService} from '../../services/all-services.service';
import {Router} from "@angular/router";
import {AppComponent} from '../../app.component';
import { env } from 'process';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-create-topic',
  templateUrl: './create-topic.page.html',
  styleUrls: ['./create-topic.page.scss'],
})
export class CreateTopicPage implements OnInit {
  cTopic:FormGroup;
  submitted = false;
  name:any;
  topictype:any;
  topictypes:any;
  level:any;
  templatename:any;
  activeMembership:Boolean = false;
  membershipStatus:any;
  constructor(private appComponent:AppComponent,private formBuilder: FormBuilder,private allService:AllServicesService,private router:Router , private toast : ToastController) { }

  ngOnInit() {
    this.cTopic= this.formBuilder.group({
      name:new FormControl('',[Validators.required ]),
      topictype:new FormControl('',[Validators.required ]),
      level:new FormControl(),
      templatename:new FormControl('',[Validators.required ]),
     });
     this.allService.getMembershipDetails().subscribe(
      (res:any)=>{
        console.log("API Responce"+JSON.stringify(res))
        this.membershipStatus = res.membsershipStatus;
        if(this.membershipStatus == 1)
        {
          this.activeMembership = true;
        }
        console.log("act",this.membershipStatus);
      }
    )
      // get template name from envirnment variable and set to the text box.
      this.cTopic.controls.templatename.setValue(localStorage.getItem('templateName'));
      this.populateTopicType();
  }
 
  ionViewDidEnter() {
    let elem = <HTMLElement>document.querySelector(".tab_hide");
    if (elem != null) {
      elem.style.display = 'none';
    }
  }

  get b8() { return this.cTopic.controls; }

  //populate topic type 
  populateTopicType() {
    this.allService.getTopicType(FormData).subscribe(
      (res: any) => {
         if (res.success) {
          this.topictypes = res.arrTopicType; 
        }
        else {
        }
      }
    )
  } 
 
  //onSubmit check valid ir invalid if valid then store topic id into envirnment and navigate ti field page.
  onSubmit1() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.cTopic.invalid) {
        return ;
    }
   //appen valid data
    const formData = new FormData();
    formData.append("topic_name",this.cTopic.value.name);
    formData.append("select_topic_type",this.cTopic.value.topictype)
    formData.append("select",this.cTopic.value.level);
    formData.append("template_name_guid",localStorage.getItem('templateCreateId'));
    formData.append("token",localStorage.getItem('token'));
    
    //post service to insert data.
    this.allService.createTopic(formData).subscribe(
      async (res:any)=>{
        console.log("API response : "+JSON.stringify(res));
        if(res.success){
          let toast = await this.toast.create({
            message: res.message,
            color:'success',
            duration: 1500,
            position: 'top',
            buttons:[{
              text:'OK',
              handler:()=>{
                console.log("OK");
              }
            }]
          });
          toast.present();
          //set topic name and id to envirnment for next process.
          // environment.topicName = this.cTopic.value.name;
          // environment.topicId = res.insert_id;
          localStorage.setItem('topicName',this.cTopic.value.name);
          localStorage.setItem('topicId',res.insert_id);
          //this.router.navigate(['/create-fields']);
          window.location.assign('/create-fields');
        }
        else{
          let toast = await this.toast.create({
            message: res.message,
            color:'danger',
            duration: 1500,
            position: 'top',
            buttons:[{
              text:'OK',
              handler:()=>{
                console.log("OK");
              }
            }]
          });
          toast.present();
          }
        }
      );
}

onReset() {
  this.submitted = false;
  this.cTopic.reset();
}

back(){
  this.router.navigate(['/select-topic']);
}
}
