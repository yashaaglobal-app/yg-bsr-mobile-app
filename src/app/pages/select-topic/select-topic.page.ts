import { ContentChild, Directive, HostListener, OnInit , Inject, Component, Renderer2} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { environment } from 'src/environments/environment';
import {AllServicesService} from '../../services/all-services.service';
import {Router} from "@angular/router";
import {AppComponent} from '../../app.component';
import { DOCUMENT } from '@angular/common';
import { ToastController } from '@ionic/angular';
import { IonSelect } from '@ionic/angular';
import { exit } from 'process';
@Component({
  selector: 'app-select-topic',
  templateUrl: './select-topic.page.html',
  styleUrls: ['./select-topic.page.scss'],
})

export class SelectTopicPage implements OnInit {
  @ContentChild(IonSelect) ionSelect;
  Topic:FormGroup;
  submitted = false;
  selectTopic:any;
  topics:any;
  selectedTopicName:any=[];
  topicId:any=[];
  selectedArray :any = []; 
  isSelected=false;
  isSelectAll=true; 
  basket = [];
  years: any[] = [];
  listener;
  selectAllCheckBox: any;
  checkBoxes: HTMLCollection;
  isChecked = false;
  topicArrayCount:any;
  len:any;
  activeMembership:Boolean = false;
  membershipStatus:any;
  // _rateSpecific:any[] = [];
  // _militaryGeneral:any[] = [];
  // _Both:any[] = [];
  customAlertOptions: any = {
    header: 'Topics',
    subHeader: 'Select All:',
    message: '<ion-checkbox id="selectAllCheckBox"></ion-checkbox>'
  };

  constructor(private appComponent:AppComponent,private formBuilder: FormBuilder,
    private allService:AllServicesService,private router:Router , private toast : ToastController,
    @Inject(DOCUMENT) private document: Document, private renderer: Renderer2) {
      }
  
  ngOnInit() {
    this.Topic=this.formBuilder.group({
      selectTopic:new FormControl('',[Validators.required]),
    });

    this.allService.getMembershipDetails().subscribe(
      (res:any)=>{
        console.log("API Responce"+JSON.stringify(res))
        this.membershipStatus = res.membsershipStatus;
        if(this.membershipStatus == 1)
        {
          this.activeMembership = true;
        }
        console.log("act",this.membershipStatus);
      }
    )
    
    //populated topics
    //this.populateTopics();  
   }
 
   openSelector(selector) {

    selector.open().then((alert)=>{
      this.selectAllCheckBox = this.document.getElementById("selectAllCheckBox");
      this.checkBoxes = this.document.getElementsByClassName("alert-checkbox");
      console.log(this.selectAllCheckBox.checked);
      this.listener = this.renderer.listen(this.selectAllCheckBox, 'click', () => {
          if (this.selectAllCheckBox.checked) {
            this.isChecked = false;
            //this.selectAllCheckBox.setAttribute('checked','true')
            for (let checkbox of this.checkBoxes) {
              if (checkbox.getAttribute("aria-checked")==="false") {
                (checkbox as HTMLButtonElement).click();
              };
            };
          } else {
            this.isChecked = true;
            for (let checkbox of this.checkBoxes) {
              if (checkbox.getAttribute("aria-checked")==="true") {
                (checkbox as HTMLButtonElement).click();
                //checkbox.setAttribute("aria-checked")==="";
                
              };
            };
          }
      });
      alert.onWillDismiss().then(()=>{
        this.listener();
      });
    })
  }
   

//onchange event push selected topic push into array.
  onChange($event) {
    
    let selectedTopicsId=[];
    let selectedTopicNameArray = []
    console.log($event.target.value);
    selectedTopicsId = $event.target.value;
    this.years.forEach(function (value) { 
      if(selectedTopicsId.includes(value.id)){
        selectedTopicNameArray.push({['topic_name']: value.name});
       } 
  });  
  this.selectedTopicName = selectedTopicNameArray;
  this.len = this.selectedTopicName.length;
  this.selectAllCheckBox = this.document.getElementById("selectAllCheckBox");

  
  if(this.years.length == this.len )
    {
      this.isChecked = true;
      this.selectAllCheckBox.setAttribute('checked','true')
    }
    else{
      this.isChecked = false;
      this.selectAllCheckBox.setAttribute('checked','true')

    }
  }

  //before ngOninit load topics
  ionViewDidEnter(){ 
    this.populateTopics();
    
      let elem = <HTMLElement>document.querySelector(".tab_hide");
      if (elem != null) {
        elem.style.display = 'none';
      }
    
   }
   //getting topics from backend and populate.
   populateTopics() {
    let select:any=[];
     this.allService.getTopics(FormData).subscribe(
      (res:any)=>{
         if(res.success){  
           res.topic.forEach(function (value) {  
                  select.push({['id']: value.topic_guid,['name']: value.topic_name,});
            });
            this.years=select;
           //console.log(res.topic._rateSpecific);
            // res.topic._rateSpecific.forEach(function (value) { 
            //       select.push({['id']: value.id,['name']: value.name,});
            // });
            // res.topic._militaryGeneral.forEach(function (value) { 
            //   select.push({['id']: value.id,['name']: value.name,});
            // });
            // res.topic._Both.forEach(function (value) { 
            //   select.push({['id']: value.id,['name']: value.name,});
            // });
            // this.years=select;
            // this._rateSpecific = res._rateSpecific;
            // this._militaryGeneral = res._militaryGeneral;
            // this._Both = res._Both;
        }
        else{
      //     res.topic.forEach(function (value) { 
            
      //       select.push({['id']: value.topic_guid,['name']: value.topic_name,});
      // });
      // this.years=select;
        }
        console.log(res);
      }
    )
  }

  get f7() { return this.Topic.controls; }

  onSubmit3() {
    this.submitted = true;
    let arr:[]; // define temp array for storing selected dropdown value
    arr = this.Topic.value; // get selcted value and store temp array
  
    this.topicId = arr['selectTopic']; //get selcted value using key    
    
   // stop here if form is invalid
    if (this.Topic.invalid) {
        return ;
    }
    //append data
    const formData = new FormData();
    for (let topic of this.topicId) {
      formData.append('selectedTopicId[]', topic);
  } 
    formData.append("tempId",localStorage.getItem('templateCreateId'));
    formData.append("token",localStorage.getItem('token'));
    //if success then reloacte to next page.
    this.allService.saveTopic(formData).subscribe(
      async (res:any)=>{
        console.log("API response : "+JSON.stringify(res));
        if(res.success){
           let toast = await this.toast.create({
            message: res.message,
            color:'success',
            duration: 1500,
            position: 'top',
            buttons:[{
              text:'OK',
              handler:()=>{
                console.log("OK");
              }
              
            }]
           
          });
          toast.present();
            //this.router.navigate([ '/select-template']); 
             window.location.assign('/select-template');         
        }
        else{
          let toast = await this.toast.create({
            message: res.message,
            color:'red',
            duration: 1500,
            position: 'top',
            buttons:[{
              text:'OK',
              handler:()=>{
                console.log("OK");
              }
            }]
          });
          toast.present();
        }
      }
    );
}

//after submit claer form
onReset() {
  this.submitted = false;
  this.Topic.reset();

}

back(){
  this.router.navigate(['/add-template']);
}
}
