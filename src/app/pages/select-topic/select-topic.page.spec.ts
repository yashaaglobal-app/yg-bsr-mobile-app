import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectTopicPage } from './select-topic.page';

describe('SelectTopicPage', () => {
  let component: SelectTopicPage;
  let fixture: ComponentFixture<SelectTopicPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTopicPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectTopicPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
