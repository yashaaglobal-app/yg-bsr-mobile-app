import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { SelectTopicPageRoutingModule } from './select-topic-routing.module';

import { SelectTopicPage } from './select-topic.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectTopicPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SelectTopicPage]
})
export class SelectTopicPageModule {}
