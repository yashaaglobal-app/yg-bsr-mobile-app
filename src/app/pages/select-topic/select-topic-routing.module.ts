import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectTopicPage } from './select-topic.page';

const routes: Routes = [
  {
    path: '',
    component: SelectTopicPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectTopicPageRoutingModule {}
