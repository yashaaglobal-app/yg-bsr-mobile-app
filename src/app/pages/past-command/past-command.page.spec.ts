import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PastCommandPage } from './past-command.page';

describe('PastCommandPage', () => {
  let component: PastCommandPage;
  let fixture: ComponentFixture<PastCommandPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PastCommandPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PastCommandPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
