import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PastCommandPage } from './past-command.page';

const routes: Routes = [
  {
    path: '',
    component: PastCommandPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PastCommandPageRoutingModule {}
