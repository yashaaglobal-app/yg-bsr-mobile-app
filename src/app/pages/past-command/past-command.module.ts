import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { PastCommandPageRoutingModule } from './past-command-routing.module';

import { PastCommandPage } from './past-command.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PastCommandPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [PastCommandPage]
})
export class PastCommandPageModule {}
