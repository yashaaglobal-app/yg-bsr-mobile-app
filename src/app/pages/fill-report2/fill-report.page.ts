import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { AllServicesService } from '../../services/all-services.service';
import { Router } from "@angular/router";
import { AppComponent } from '../../app.component';
import { IonSlides } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { exit } from 'process';
import { ElementRef } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Message } from '@angular/compiler/src/i18n/i18n_ast';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { data } from 'jquery';
declare var jQuery: any;

import * as JSEncrypt from '../../../assets/js/jsencrypt/jsencrypt.min.js';
import * as moment from '../../../assets/js/moment.min.js';
 
@Component({
  selector: 'app-fill-report',
  templateUrl: './fill-report.page.html',
  styleUrls: ['./fill-report.page.scss'],
})
export class FillReportPage implements OnInit {

  @ViewChild('mySlider') slides: IonSlides;
  FillReport:FormGroup;
  templateData: any;
  topicData: any;
  options: any;
  name = 'Angular';
  len: any;
  template_id: any;
  topic_id:any;
  Topic:any=[];
  tempIndex = 0;
  topicIndex =0;
  inputData:any=[];
  token:any;
  dailyUpdateId:any;
  totalCount:any;
  reportSummary:any=[];
  selectedValue:Boolean = false;
  replytype:any;
  crypt:any;

  constructor(private appComponent: AppComponent, private formBuilder: FormBuilder,
    private allService: AllServicesService, private routerR: Router,private _host: ElementRef,private toast: ToastController) {
    this.template_id = localStorage.getItem('template_id');
   
    this.topic_id =localStorage.getItem('topicId');     
    // this.fillTemplate(this.template_id);//TODO 

   }

  
  ngOnInit() {

    this.crypt = new JSEncrypt();
    this.crypt.setPrivateKey(localStorage.getItem('privKey'));

    this.template_id =localStorage.getItem('template_id');
     
    this.dailyUpdateId = localStorage.getItem('dailyUpdateId');
    this.FillReport = this.formBuilder.group({
      answer: [''],
    }
    );
    this.token = localStorage.getItem('token');    
  }

  ionViewWillEnter()
  {
    this.FillReport = this.formBuilder.group({
      answer: [''],
    }
    );
    this.token = localStorage.getItem('token');    

    this.template_id = localStorage.getItem('template_id');
   
    this.topic_id =localStorage.getItem('topicId');    
    // console.log(this.topic_id); 
    this.fillTemplate(this.template_id);//TODO 
    // console.log("inside ionViewDidEnter: template selected id is"+this.template_id);

    
      let elem = <HTMLElement>document.querySelector(".tab_hide");
      if (elem != null) {
        elem.style.display = 'none';
      }
    
  }


  back(){
    this.routerR.navigate(['/select-template']);
  
  }
 
  fillTemplate(template_id: any) {
    let selectT = localStorage.getItem('selectedTopic'); // environment.selectedTopic;
    let selectedTopicNameArray = [];
    const formData = new FormData();
    this.allService.getFillTemplate(template_id).subscribe(
      (res: any) => {
        if (res.success) {
          this.templateData = res.templateData;
          //check selectT topic_guid == topic data topic_guid same guid should display on fill report page.
        //  console.log(res.topicData);
          res.topicData.forEach(function (value) { 
            // console.log('Topic value: '+value.topic_guid);
            if(selectT.includes(value.topic_guid)){
              selectedTopicNameArray.push({['guid']: value.guid,['topic_guid']:value.topic_guid,['topic_name']: value.topic_name,['fields']:value.fields});
              // console.log(selectedTopicNameArray);
            } 
        });

        this.topicData=selectedTopicNameArray;
          this.options = res.options;
           this.len = this.topicData.length;
           this.totalCount = this.topicData.length;
          //  console.log(this.totalCount);
        }
        else {          
         
        }
      }
    )
  }

  //on next click check valid,then go to next slide.
  next(id: any) {
   
    this.topicIndex=this.topicIndex + 1;
      (function ($) {
        //var temp = [];
  //  console.log(temp);
      $.fn.validate(id);

    })(jQuery);

    //data valid then slide next.
    if (localStorage.getItem("allowContinue") != 'no') {
      this.len = this.len - 1;
    if(localStorage.getItem("insert")=='yes'){
      (function ($) { 
         $('#'+id).click(); //same as calling savemore(id)
        })(jQuery);
    }
      //if(this.len==1)
      this.slides.slideNext();
    }
    //invalid then present toaster.
    else {
      (function ($) {
       
        async function presentToast1() {
          const toast = document.createElement('ion-toast');
          toast.message = 'please enter all fields or none';
          toast.duration = 3000;
          toast.position='top';
          toast.color = 'danger';
          document.body.appendChild(toast);
          return toast.present();
          
        }
        presentToast1();
        //var temp= [];
       
        //assign guid to the every next button
        var  closetParent = document.querySelector('#tab_'+id);
          console.log(closetParent);
          var slideTabElements;
          slideTabElements= closetParent.querySelectorAll('.input');

         
          //Iterate all the input under slide
          $( slideTabElements ).each(function( index ) {
            console.log("set error:"+ index + ": " + $( this ).val() );
            //check value is empty or not selected 
            if( $( this ).val()=="" ){ 
              $( this).addClass("invalid");
              
            }
            else
            {
              $( this).removeClass("invalid")
              
            }
           
          });
      })(jQuery);
    }


  }

  //slide prev
  prev() {
    this.topicIndex = this.topicIndex - 1;
    console.log("Topic Index(prev):" +this.topicIndex);
    (function ($) {
 
    })(jQuery);
    this.len = this.len + 1;
    this.slides.slidePrev();
    // todo if there is nothing at end then don''t show
  }

  //save data and add more.
  savemore(id: any){   
    //debugger;
    let defineTemp:any = [];
    var length1 = this.len;
    let isValid:any;
    var formdata = $('#dailyUpdate_'+id).serializeArray();
    var formdata1 = this.serialize_with_encode(formdata);
  
    // console.clear();
    // console.log($('#dailyUpdate_'+id).serialize());
    // console.log(formdata1);

    (function ($) {
      $.fn.highlightRequiredField(id);
      if(localStorage.getItem("allfieldNotEmpty")=="true"){
        // insertData(id);
        $.ajax({
          type: "POST",
          url: environment.baseUrl + "YashaaFileReport/insertFileReport",
          dataType: 'json',
          data: formdata1,
          success: function (resp) {
             if (resp.success) {
              presentToast(resp.message,'success');
              $(".dailyUpdateId").val(resp.intDailyUpdateId);
                localStorage.setItem("intDailyUpdateId",  resp.intDailyUpdateId);
                localStorage.setItem("DUInsertsuccess","yes");
                (function ($) {
                  $('.otherBoxes').css({"display":"none"});
                  $('.otherBoxesInput').attr('data-field-required','no');
                  $('.otherBoxesInput').val('');
                })(jQuery);
             } else {
              presentToast(resp.message, 'danger'); 
            }
          },
          error: function (xhr, status, error) {
            presentToast(error, 'danger');
          }
        });
       
      }
      else{
        presentToast('please enter all fields or none','danger');
      }

  async function presentToast(message,color) {
       
        const toast = document.createElement('ion-toast');
        toast.message = message;
        toast.duration = 3000;
        toast.position='top';
        toast.color = color;
        document.body.appendChild(toast);
        return toast.present();
        
      }
     
    })(jQuery);

      this.inputData.push(defineTemp);
      console.log(this.inputData);
    
    //debugger;
    if(localStorage.getItem("allfieldNotEmpty")=="true"){
     
      this.onReset(); 
    }
  }

  //clear form
  onReset() {
    this.FillReport.reset();
    this.FillReport = this.formBuilder.group({
      answer: [''],
    }
    );
  }

  finish(id:any){
    
    (function ($) {
  
    $.fn.validate(id);

  })(jQuery);

  if (localStorage.getItem("allowContinue") != 'no') {
  
    if(localStorage.getItem("insert")=='yes'){
        this.savemore(id);
          }
    
      window.location.replace("/dashboard");
   
  }
  else{
    alert("fill all fields");
  }
   localStorage.setItem("topic_id","")   //TODO what is use of this?
  }
  
  //remove valid invalid class
  removeClass(){
    (function ($) {
      $( this).removeClass("invalid")
    })(jQuery);
  }

  showOther(event, id:any)
  { (function ($) {
    $('#otheInput_'+id).val('');
      if(event.target.value == "Other")
      {
        $('#otheInput_'+id).attr('data-field-required','yes');
        $('#showBox_'+id).css({"display":"block"});
      }
      else{
        $('#otheInput_'+id).val(event.target.value);
        $('#otheInput_'+id).attr('data-field-required','no');
        $('#showBox_'+id).css({"display":"none"});
      }
    })(jQuery);
  }

  // serialize_with_encode(formdata, arr_except=[]) {
  //   var str = "";
  //   formdata.forEach( (obj) => {
  //     var inpName= obj.name.substr(0, obj.name.indexOf('['));
  //     if(inpName == 'answer') {
  //       var day = moment(obj.value, "MM-DD-YYYY");
  //       if(day.format() == "Invalid date") {
  //           var enc = encodeURIComponent(this.YashaaEncrypt(obj.value));
  //           str += obj.name + "=" + enc + "&";
  //       }else{
  //           str += obj.name + "=" + obj.value+ "&";
  //       }
  //     }else{
  //       str += obj.name + "=" + obj.value+ "&";
  //     }
  //   });
  //   return str;
  // }

  serialize_with_encode(formdata, arr_except=[]) {
    var str = "";
    // console.log(formdata);
    formdata.forEach( (obj) => {
      var inpName = obj.name.substr(0, obj.name.indexOf('['));
      if(inpName == 'answer') {
        // var day = new Date(obj.value);
        // console.log(obj.value, day, '------------');
        // var day = moment(obj.value, "MM-DD-YYYY");

        // console.log($("input[name='"+obj.name+"']").data('field-guid'),obj.value);

        var field_type = $("input[name='"+obj.name+"']").data('field-guid'); 
        if(field_type != 3 && field_type != 4) {
            var objVal = this.YashaaEncrypt(obj.value);
            var enc = encodeURIComponent(objVal);
            str += obj.name + "=" + enc + "&";
        } else { 
            str += obj.name + "=" + obj.value+ "&";
        } 
      } else {
        str += obj.name + "=" + obj.value+ "&";
      } 
    });
    return str;
  }

  YashaaEncrypt(input) {
    return this.crypt.encrypt(input);
  }

  YashaaDecrypt(crypted) {
    return this.crypt.decrypt(crypted);
  }
  

}
