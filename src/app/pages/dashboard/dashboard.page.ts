import { Component, OnInit } from '@angular/core';
import { AllServicesService } from 'src/app/services/all-services.service';
import {AppComponent} from '../../app.component';

import * as JSEncrypt from '../../../assets/js/jsencrypt/jsencrypt.min.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  token:any;
  Username:any;
  crypt:any;

  constructor(private appComponent:AppComponent ,private allService : AllServicesService) { }

  ngOnInit() {
    this.crypt = new JSEncrypt();
    this.token = localStorage.getItem('token');
    this.appComponent.initializeApp();
    this.Username=localStorage.getItem('username');

    if((localStorage.getItem('privKey') === '' || localStorage.getItem('privKey') == 'null') && (localStorage.getItem('pubKey') === '' || localStorage.getItem('pubKey') == 'null')) {
      this.generateKeys()
    }
  }

  generateKeys() {
    var keySize = 512;
    this.crypt = new JSEncrypt({default_key_size: keySize});
    this.updateUserKeys();
    this.crypt.getKey(function () {
      
        // this.storage.set('pubKey', crypt.getPublicKey());
        // this.storage.set('privKey', crypt.getPrivateKey());
    });
    this.crypt.getKey();
    return true;
  }

  updateUserKeys() {

    var privKey = this.crypt.getPrivateKey();
    var pubKey = this.crypt.getPublicKey();

    const formData = new FormData();
    formData.append("privKey",privKey);
    formData.append("pubKey",pubKey);
    formData.append("token",localStorage.getItem('token'));

    this.allService.updateUserKeys(formData).subscribe((res:any)=>{
      localStorage.setItem('pubKey', pubKey);
      localStorage.setItem('privKey', privKey);
      // this.appComponent.showAlert('Updated',res.message);
    }),
    (err:any)=>
    {
      // this.appComponent.showAlert('Failed',err.message);
    }
  }

     
}
