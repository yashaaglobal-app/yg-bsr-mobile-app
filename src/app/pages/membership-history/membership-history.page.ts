import { Component, OnInit } from '@angular/core';
import { AllServicesService } from 'src/app/services/all-services.service';
import {Router} from "@angular/router";
declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-membership-history',
  templateUrl: './membership-history.page.html',
  styleUrls: ['./membership-history.page.scss'],
})
export class MembershipHistoryPage implements OnInit {
  membershipHistoy : any;
  constructor( 
    private allService : AllServicesService,private router:Router
    ) {}

  ngOnInit() {
    this.fetchMembershipHistory();
  }

  fetchMembershipHistory(){
    this.allService.getMembershipHistory().subscribe(
      (res:any)=>{
         console.log("API Responce"+JSON.stringify(res))
          this.membershipHistoy=res.membership_history;
          console.log(this.membershipHistoy);
      }
    )
  }
  back(){
    this.router.navigate(['/user-profile']);
  }

}
