import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MembershipHistoryPageRoutingModule } from './membership-history-routing.module';

import { MembershipHistoryPage } from './membership-history.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MembershipHistoryPageRoutingModule
  ],
  declarations: [MembershipHistoryPage]
})
export class MembershipHistoryPageModule {}
