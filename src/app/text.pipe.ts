import { Pipe, PipeTransform } from "@angular/core";

@Pipe({ name: "texttransform" })
export class TextTransformPipe implements PipeTransform {
  transform(value: string): string {
    const splitBy = "::";
    const splittedText = value.split(splitBy);
    if (splittedText.length > 1) {
        return `${"Start Date: <br/> " +
        splittedText[0] +
        " <br/> End Date: <br/>" +
        splittedText[1]}`;
    } else {
      return `${splittedText[0]}`;
    }
  }
}